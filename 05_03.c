#include <stdio.h>

static char *
StringCat(char *Source, char *Dest)
{
	char *SourcePtr = Source;
	char *DestPtr = Dest;

	while(*SourcePtr++ != 0);
	--SourcePtr;

	while((*SourcePtr++ = *DestPtr++) != 0);

	return Source;
}

int main(void)
{
	char First[100] = "Hello ";
	char Second[100] = "World\n";

	StringCat(First, Second);
	printf("%s", First);

	return 0;
}
