#include <stdio.h>

#define MAX_CHARACTERS 127
#define MAX_LENGTH 10

int main(void)
{
	int Character = 0;
	int Characters[MAX_CHARACTERS];
	for(int i = 0; i < MAX_CHARACTERS; ++i)
	{
		Characters[i] = 0;
	}

	while((Character = getchar()) != EOF)
	{
		++Characters[Character];
	}

	int Max = -1;
	for(int CharacterIndex = 0; CharacterIndex < MAX_CHARACTERS; ++CharacterIndex)
	{
		if(Characters[CharacterIndex] > Max)
		{
			Max = Characters[CharacterIndex];
		}
	}

	for(int CharacterIndex = 0; CharacterIndex < MAX_CHARACTERS; ++CharacterIndex)
	{
		float Percent = (float)(Characters[CharacterIndex]) / (float)Max;
		float Length = (float)MAX_LENGTH * Percent;
		printf("%c: ", (char)CharacterIndex);
		for(int i = 0; i < (int)(Length + 0.5); ++i)
		{
			printf("*");
		}
		printf("\n");
	}

	return 0;
}
