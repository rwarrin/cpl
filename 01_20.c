#include <stdio.h>

#define LINELENGTH 1000
#define INDENTSIZE 4

int
GetLine(char Buffer[], int Limit)
{
	int Length = 0;
	int Character = 0;

	while((Character = getchar()) != EOF)
	{
		if(Character == '\t')
		{
			for(int i = 0; i < INDENTSIZE; ++i)
			{
				Buffer[Length++] = ' ';
			}
		}
		else
		{
			Buffer[Length++] = Character;
		}
		if((Character == '\n') ||
		   (Length == Limit - 2))
		{
			break;
		}
	}

	if(Length > 0)
	{
		Buffer[Length++] = '\0';
	}

	return Length;
}

int main(void)
{
	char Buffer[LINELENGTH];
	int Length = 0;
	while((Length = GetLine(Buffer, LINELENGTH)) > 0)
	{
		printf("%s", Buffer);
	}
	return 0;
}
