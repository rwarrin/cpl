#include <stdio.h>

float
CToF(float Temp)
{
	float Result = (9.0f/5.0f)*Temp + 32.0f;
	return(Result);
}

float
FToC(float Temp)
{
	float Result = (5.0 / 9.0) * (Temp - 32.0);
	return(Result);
}

#define LOWER 0
#define UPPER 300
#define STEP  20

int main(void)
{
	printf("%3c %6c %3c %3c\n", 'F', 'C', 'C', 'F');
	for(int Temp = LOWER; Temp <= UPPER; Temp += STEP)
	{
		printf("%3.0d %6.1f %3.0d %3.0f\n", Temp, FToC(Temp), Temp, CToF(Temp));
	}

	return 0;
}
