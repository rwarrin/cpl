#include <stdio.h>

static int
StringEnd(char *Source, char *Find)
{
	int Result = 0;

	char *SourceEnd = Source;
	char *FindEnd = Find;

	// Get the NULL terminating character index
	while(*SourceEnd++ != 0);
	while(*FindEnd++ != 0);

	while((*FindEnd-- == *SourceEnd--) &&
		  (FindEnd > Find));

	if(FindEnd == Find)
	{
		Result = 1;
	}

	return(Result);
}

int main(void)
{

	char *Source = "This is the test string";
	char *Find = "ring";
	int Result = StringEnd(Source, Find);

	printf("%d\n", Result);

	return 0;
}
