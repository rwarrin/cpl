#if !defined(WRITER_C)

// TODO(rick): This is probably not neccessary... will probably remove this soon
enum replaceable_token_type
{
	ReplaceableTokenType_Documentation,
	ReplaceableTokenType_Name,
	ReplaceableTokenType_Description,
	ReplaceableTokenType_Arguments,
	ReplaceableTokenType_Return,
	ReplaceableTokenType_Signature,
	ReplaceableTokenType_Index,
};

struct replaceable_token
{
	char *Start;
	char *End;
	int Length;
};

struct replaceable_tokens
{
	enum replaceable_token_type Type;
	int TokenCount;
	struct replaceable_token Tokens[16];
};

struct documentation_item
{
	char *Description;
	int DescriptionLength;

	char *Params[32];  // function with more than 32 params? no..
	int ParamsLength[32];
	int ParamsCount;

	char *Return;
	int ReturnLength;

	char *Signature;
	int SignatureLength;

	char *Name;
	int NameLength;
};

struct documentation_writer
{
	struct file_result PageTemplate;
	struct file_result DocumentationTemplate;

	struct replaceable_tokens Tokens[16];
	int TokensCount;

	struct documentation_item DocItems[100];
	int DocItemsCount;
};

static struct documentation_writer
Documentation_Init()
{
	struct documentation_writer Writer = {0};

	return(Writer);
}

static void
Documentation_End(struct documentation_writer *Writer)
{
	if(Writer)
	{
		if(Writer->PageTemplate.FileContents)
		{
			RemoveFileFromMemory(&Writer->PageTemplate);
		}

		if(Writer->DocumentationTemplate.FileContents)
		{
			RemoveFileFromMemory(&Writer->DocumentationTemplate);
		}
	}
}

static struct replaceable_tokens
GetTemplateTokens(char *Text, char *TokenTag)
{
	struct replaceable_tokens Result = {0};

	int TokenLength = StringLength(TokenTag);
	while(*Text != 0)
	{
		if((*Text == '[') &&
		   (*(Text + 1) == '['))
		{
			char *TempToken = TokenTag;
			char *TempText = Text + 2;
			while((*TempToken != 0) &&
				  (*TempText == *TempToken))
			{
				++TempText, ++TempToken;
			}

			if(*TempToken == 0)
			{
				Result.Tokens[Result.TokenCount].Start = Text;
				// NOTE(rick): 4 is to account for the [[ and ]] characters.
				Result.Tokens[Result.TokenCount].End = Text + TokenLength + 4;
				Result.Tokens[Result.TokenCount].Length = Result.Tokens[Result.TokenCount].End - Result.Tokens[Result.TokenCount].Start;
				Text += Result.Tokens[Result.TokenCount].Length;
				++Result.TokenCount;
			}
		}

		++Text;
	}

	return(Result);
}

static void
Documentation_SetPageTemplate(struct documentation_writer *Writer, char *FileName)
{
	if(Writer)
	{
		Writer->PageTemplate = ReadEntireFileIntoMemory(FileName);
		if(Writer->PageTemplate.FileContents)
		{
			struct replaceable_tokens *Tokens = &Writer->Tokens[Writer->TokensCount];
			*Tokens = GetTemplateTokens(Writer->PageTemplate.FileContents, "DOCUMENTATION");
			Tokens->Type = ReplaceableTokenType_Documentation;
			++Writer->TokensCount;

			Tokens = &Writer->Tokens[Writer->TokensCount];
			*Tokens = GetTemplateTokens(Writer->PageTemplate.FileContents, "INDEX");
			Tokens->Type = ReplaceableTokenType_Index;
			++Writer->TokensCount;
		}
		else
		{
			fprintf(stderr, "Failed to set page template '%s'\n", FileName);
		}
	}
}

static void
Documentation_SetDocTemplate(struct documentation_writer *Writer, char *FileName)
{
	if(Writer)
	{
		Writer->DocumentationTemplate = ReadEntireFileIntoMemory(FileName);
		if(Writer->DocumentationTemplate.FileContents)
		{
			// TODO(rick): Roll the type up into the function call
			struct replaceable_tokens *Tokens = &Writer->Tokens[Writer->TokensCount];
			*Tokens = GetTemplateTokens(Writer->DocumentationTemplate.FileContents, "TOKEN_NAME");
			Tokens->Type = ReplaceableTokenType_Name;
			++Writer->TokensCount;

			Tokens = &Writer->Tokens[Writer->TokensCount];
			*Tokens = GetTemplateTokens(Writer->DocumentationTemplate.FileContents, "TOKEN_DESCRIPTION");
			Tokens->Type = ReplaceableTokenType_Description;
			++Writer->TokensCount;

			Tokens = &Writer->Tokens[Writer->TokensCount];
			*Tokens = GetTemplateTokens(Writer->DocumentationTemplate.FileContents, "TOKEN_SIGNATURE");
			Tokens->Type = ReplaceableTokenType_Signature;
			++Writer->TokensCount;

			Tokens = &Writer->Tokens[Writer->TokensCount];
			*Tokens = GetTemplateTokens(Writer->DocumentationTemplate.FileContents, "TOKEN_RETURN");
			Tokens->Type = ReplaceableTokenType_Return;
			++Writer->TokensCount;

			Tokens = &Writer->Tokens[Writer->TokensCount];
			*Tokens = GetTemplateTokens(Writer->DocumentationTemplate.FileContents, "TOKEN_ARGUMENTS");
			Tokens->Type = ReplaceableTokenType_Arguments;
			++Writer->TokensCount;
		}
		else
		{
			fprintf(stderr, "Failed to set documentation template '%s'\n", FileName);
		}
	}
}

static void
_WriteDocumentationItems(struct documentation_writer *Writer, FILE *FileOut)
{
	for(int DocItemIndex = 0;
		DocItemIndex < Writer->DocItemsCount;
		++DocItemIndex)
	{
		char *DocTemplateAt = Writer->DocumentationTemplate.FileContents;
		while(*DocTemplateAt != 0)
		{
			if(*DocTemplateAt == '[')
			{
				bool32 Found = false;
				for(int TokenIndex = 0;
					TokenIndex < Writer->TokensCount && !Found;
					++TokenIndex)
				{
					// TODO(rick): We're on that nested arrays life.. we should
					// probably think of a better way to do this at some point
					for(int SubTokenIndex = 0;
						SubTokenIndex < Writer->Tokens[TokenIndex].TokenCount && !Found;
						++SubTokenIndex)
					{
						struct replaceable_tokens *ReplaceableTokens = &Writer->Tokens[TokenIndex];
						struct replaceable_token *ReplaceableToken = &ReplaceableTokens->Tokens[SubTokenIndex];

						if(DocTemplateAt == ReplaceableToken->Start)
						{
							printf("Found a replaceable token in the doc template...\n");
							Found = true;

							// TODO(rick): Break this out into a new function
							// where we can pass the replaceable token, the
							// DocItem, and the File to write the documentation
							// to...
							switch(ReplaceableTokens->Type)
							{
								case ReplaceableTokenType_Name:
								{
									printf("Writing a name.\n");
									fwrite(Writer->DocItems[DocItemIndex].Name, 1,
										Writer->DocItems[DocItemIndex].NameLength, FileOut);
									DocTemplateAt += ReplaceableToken->Length;
								} break;

								case ReplaceableTokenType_Description:
								{
									printf("Writing a description.\n");
									fwrite(Writer->DocItems[DocItemIndex].Description, 1,
										Writer->DocItems[DocItemIndex].DescriptionLength, FileOut);
									DocTemplateAt += ReplaceableToken->Length;
								} break;

								case ReplaceableTokenType_Arguments:
								{
									printf("Writing a arguments.\n");
									for(int ArgumentIndex = 0;
										ArgumentIndex < Writer->DocItems[DocItemIndex].ParamsCount;
										++ArgumentIndex)
									{
										fwrite(Writer->DocItems[DocItemIndex].Params[ArgumentIndex], 1,
											Writer->DocItems[DocItemIndex].ParamsLength[ArgumentIndex], FileOut);
									}

									DocTemplateAt += ReplaceableToken->Length;
								} break;

								case ReplaceableTokenType_Return:
								{
									printf("Writing a return.\n");
									fwrite(Writer->DocItems[DocItemIndex].Return, 1,
										Writer->DocItems[DocItemIndex].ReturnLength, FileOut);
									DocTemplateAt += ReplaceableToken->Length;
								} break;

								case ReplaceableTokenType_Signature:
								{
									printf("Writing a signature.\n");
									fwrite(Writer->DocItems[DocItemIndex].Signature, 1,
										Writer->DocItems[DocItemIndex].SignatureLength, FileOut);
									DocTemplateAt += ReplaceableToken->Length;
								} break;

								InvalidDefaultCase;
							}
						}
					}
				}
			}

			fputc(*DocTemplateAt, FileOut);
			++DocTemplateAt;
		}

	}
}

static void
Documentation_Write(struct documentation_writer *Writer, char *FileName)
{
	FILE *FileOut = fopen(FileName, "wb");
	if(FileOut)
	{
		char *PageTemplateAt = Writer->PageTemplate.FileContents;
		while(*PageTemplateAt != 0)
		{
			for(int TokenIndex = 0;
				TokenIndex < Writer->TokensCount;
				++TokenIndex)
			{
				if(PageTemplateAt == Writer->Tokens[TokenIndex].Tokens[0].Start)
				{
					switch(Writer->Tokens[TokenIndex].Type)
					{
						case ReplaceableTokenType_Documentation:
						{
							printf("Found a replace token...\n");
							// TODO(rick): Switch writing context to something else
							// because we just found a token
							PageTemplateAt += Writer->Tokens[TokenIndex].Tokens[0].Length;
							_WriteDocumentationItems(Writer, FileOut);
						} break;

						case ReplaceableTokenType_Index:
						{
							printf("Found a Index token...\n");
							// TODO(rick): Switch writing context to something else
							// because we just found a token
							PageTemplateAt += Writer->Tokens[TokenIndex].Tokens[0].Length;

							char *IndexListBegin = "<ul>";
							int IndexListBeginLength = StringLength(IndexListBegin);
							char *IndexListEnd = "</ul>";
							int IndexListEndLength = StringLength(IndexListEnd);

							char *IndexListItemBegin = "<li><a href=\"#";
							int IndexListItemBeginLength = StringLength(IndexListItemBegin);
							char *IndexListItemMid = "\">";
							int IndexListItemMidLength = StringLength(IndexListItemMid);
							char *IndexListItemEnd = "</a></li>";
							int IndexListItemEndLength = StringLength(IndexListItemEnd);

							fwrite(IndexListBegin, 1, IndexListBeginLength, FileOut);
							for(int DocItemIndex = 0;
								DocItemIndex < Writer->DocItemsCount;
								++DocItemIndex)
							{
								struct documentation_item *DocItem = (Writer->DocItems + DocItemIndex);

								fwrite(IndexListItemBegin, 1, IndexListItemBeginLength, FileOut);
								fwrite(DocItem->Name, 1, DocItem->NameLength, FileOut);
								fwrite(IndexListItemMid, 1, IndexListItemMidLength, FileOut);
								fwrite(DocItem->Name, 1, DocItem->NameLength, FileOut);
								fwrite(IndexListItemEnd, 1, IndexListItemEndLength, FileOut);
							}
							fwrite(IndexListEnd, 1, IndexListEndLength, FileOut);
						} break;

						InvalidDefaultCase;
					}
				}
			}

			fputc(*PageTemplateAt, FileOut);
			++PageTemplateAt;
		}

		fclose(FileOut);
	}
	else
	{
		fprintf(stderr, "Could not write to file %s\n", FileName);
	}
}

#define WRITER_C
#endif
