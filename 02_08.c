#include <stdio.h>

int RightRot(int Value, int Amount)
{
	int Result = 0;

	Result = (Value >> Amount) | (Value << (32 - Amount));

	return(Result);
}

int main(void)
{
	unsigned int Result = RightRot(0x0F, 2);
	printf("%0x, %u\n", Result, Result);
	return 0;
}
