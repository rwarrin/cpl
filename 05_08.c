#include <stdio.h>

static char daytab[2][13] = 
{
	{0, 31, 28, 31, 30, 31, 30, 31, 31, 30 ,31, 30, 31},
	{0, 31, 29, 31, 30, 31, 30, 31, 31, 30 ,31, 30, 31},
};

int
day_of_year(int year, int month, int day)
{
	int i, leap;
	leap = (((year % 4 == 0) &&
			(year % 100 != 0)) ||
			(year % 400 == 0));

	if((year < 0) ||
	  (month < 0) ||
	  (month > 12) ||
	  (day < 0) ||
	  (day > daytab[leap][month]))
	{
		return -1;
	}

	for(i = 1; i < month; ++i)
	{
		day += daytab[leap][i];
	}

	return day;
}

void
month_day(int year, int yearday, int *pmonth, int *pday)
{
	int i, leap;

	leap = (((year % 4 == 0) &&
			(year % 100 != 0)) ||
			(year % 400 == 0));
	if((year < 0) ||
	   (yearday < 0) ||
	   (leap && (yearday > 366)) ||
	   (!leap && (yearday > 364)))
	{
		*pmonth = -1;
		*pday = -1;
		return;
	}

	for(i = 1; yearday > daytab[leap][i]; ++i)
	{
		yearday -= daytab[leap][i];
	}

	*pmonth = i;
	*pday = yearday;
}

int main(void)
{
	printf("Day of year: %d\n", day_of_year(2016, 8, 27));

	int Month = 0;
	int Day = 0;
	int Year = 2016;
	month_day(Year, 240, &Month, &Day);
	printf("%d/%d/%d\n", Month, Day, Year);

	return 0;
}
