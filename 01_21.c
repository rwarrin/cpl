#include <stdio.h>

#define LINELENGTH 1000
#define SPACES 4

int
GetLine(char Buffer[], int Limit)
{
	int Length = 0;
	int Character = 0;

	while((Character = getchar()) != EOF)
	{
		if(Character == ' ')
		{
			int Spaces = 1;
			while((Character = getchar()) == ' ')
			{
				++Spaces;
			}

			int Tabs = Spaces / SPACES;
			int Leftover = Spaces % SPACES;

			for(int i = 0; i < Tabs; ++i)
			{
				Buffer[Length++] = '\t';
			}
			for(int i = 0; i < Leftover; ++i)
			{
				Buffer[Length++] = ' ';
			}

			Buffer[Length++] = Character;
		}
		else
		{
			Buffer[Length++] = Character;
		}

		if(Character == '\n')
		{
			break;
		}
	}

	if(Length > 0)
	{
		Buffer[Length++] = '\0';
	}

	return(Length);
}

int main(void)
{
	char Buffer[LINELENGTH];
	int Length = 0;

	while((Length = GetLine(Buffer, LINELENGTH)) > 0)
	{
		printf("%s", Buffer);
	}

	return 0;
}
