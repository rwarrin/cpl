#include <stdio.h>

int
Invert(int In, int Position, int Count)
{
	int Result = In;

	for(int Index = 0; Index < 32; ++Index)
	{
		if((Index <= Position - 1) &&
		   (Index > Position - 1 - Count))
		{
			if((Result & (1 << Index)) != 0)
			{
				Result = Result ^ (1 << Index);
			}
			else
			{
				Result = Result | (1 << Index);
			}
		}
	}


	return(Result);
}

int main(void)
{
	int Result = Invert(0x00, 6, 4);
	printf("%d\n", Result);

	Result = Invert(0xFF, 6, 4);
	printf("%d\n", Result);

	return 0;
}
