#include <stdio.h>

int main(void)
{
	int Character = 0;
	while((Character = getchar()) != EOF)
	{
		if(Character == '\t')
		{
			putchar('\\');
			putchar('t');
		}
		else if(Character == '\b')
		{
			putchar('\\');
			putchar('b');
		}
		else if(Character == '\\')
		{
			putchar('\\');
			putchar('\\');
		}
		else
		{
			putchar(Character);
		}
	}

	return 0;
}
