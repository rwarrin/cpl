#include <stdio.h>

int main(void)
{
	printf("%3s %6s\n", "F", "C");
	for(int fahr = 300; fahr >= 0; fahr = fahr - 20)
	{
		printf("%3d %6.1f\n", fahr, (5.0f / 9.0f)*(fahr - 32.0));
	}

	return 0;
}
