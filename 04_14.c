#include <stdio.h>

#define Swap(type, A, B) { type Temp = A; A = B; B = Temp; }

int main(void)
{
	int A = 1;
	int B = 2;
	printf("A = %d, B = %d\n", A, B);
	Swap(int, A, B);
	printf("A = %d, B = %d\n", A, B);

	float X = 3.0f;
	float Y = 4.0f;
	printf("A = %.3f, B = %.3f\n", X, Y);
	Swap(float, X, Y);
	printf("A = %.3f, B = %.3f\n", X, Y);

	return 0;
}
