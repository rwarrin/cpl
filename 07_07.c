#include <stdio.h>
#include <stdlib.h>

#define MAX_LINE 1000

static int GlobalIgnoreCase;

static int
StringContainsString(char *Source, char *Pattern)
{
	char *SourceStart = Source;

	while(*Source != 0)
	{
		// TODO(rick): Implement ignore case in the search
		if(*Source == *Pattern)
		{
			int Found = 1;
			for(int Index = 0;
				(*(Pattern + Index) != 0) && Found;
				++Index)
			{
				if(*(Source + Index) != *(Pattern + Index))
				{
					Found = 0;
				}
			}

			if(Found)
			{
				return(Source - SourceStart);
			}
		}

		++Source;
	}

	return(-1);
}

#define IsWhitespace(Character) ((Character == ' ') || (Character == '\t'))

static char *
RemovePreceedingWhitespace(char *String)
{
	while(IsWhitespace(*String))
	{
		++String;
	}

	return(String);
}

int
main(int Argc, char **Argv)
{
	if(Argc < 3)
	{
		printf("Usage: %s [list of files] [pattern to match]\n", Argv[0]);
		return 1;
	}

	// TODO(rick): Remove global variables and move options into a struct that
	// can be passed around or made global...
	GlobalIgnoreCase = 0;

	// Find and set program switches before running
	for(int ArgIndex = 1;
		ArgIndex < Argc - 1;
		++ArgIndex)
	{
		char *Argument = Argv[ArgIndex];
		if(*Argument == '-')
		{
			while(*++Argument != 0)
			{
				switch(*Argument)
				{
					case 'i':
					{
						GlobalIgnoreCase = 1;
					} break;
					default:
					{
						fprintf(stderr, "Unkown switch '%c'\n", *Argument);
					} break;
				}
			}
		}
	}

	char *Pattern = Argv[Argc - 1];
	char ReadLine[MAX_LINE];

	for(int ArgIndex = 1;
		ArgIndex < Argc - 1;
		++ArgIndex)
	{
		char *Filename = Argv[ArgIndex];
		FILE *File = fopen(Filename, "rb");
		if(File)
		{
			int LineNumber = 0;
			while(fgets(ReadLine, MAX_LINE, File) != 0)
			{
				++LineNumber;

				if(StringContainsString(ReadLine, Pattern) != -1)
				{
					printf("%s:%d:%s", Filename, LineNumber,
						   RemovePreceedingWhitespace(ReadLine));
				}
			}

			fclose(File);
		}
	}

	return 0;
}
