#include <stdio.h>

#define ArrayCount(Array) (sizeof(Array) / sizeof((Array)[0]))

#define BUFFERLENGTH 1000

int
GetLine(char Buffer[], int Limit)
{
	int Length = 0;
	int Character = 0;

	while((Character = getchar()) != EOF)
	{
		if(Character == '\n')
		{
			break;
		}

		Buffer[Length++] = Character;
	}

	if(Length > 0)
	{
		Buffer[Length++] = 0;
	}

	return(Length);
}

long
HToI(char String[])
{
	int StringIndex = 0;
	if((String[0] == '0') &&
	   ((String[1] == 'x') ||
		(String[1] == 'X')))
	{
		StringIndex = 2;
	}

	long Result = 0;
	for(; String[StringIndex] != 0; ++StringIndex)
	{
		int Character = String[StringIndex];
		int Value = 0;

		if((Character >= '0') && (Character <= '9'))
		{
			Value = (Character - '0');
		}
		else if((Character >= 'a') && (Character <= 'f'))
		{
			Value = 10 + (Character - 'a');
		}
		else if((Character >= 'A') && (Character <= 'F'))
		{
			Value = 10 + (Character - 'A');
		}
		else
		{
			// NOTE(rick): Invalid Hex character encountered so we'll just
			// return a sentinal value indicating an error
			return -1;
		}

		Result = Result * 16 + (Value);
	}

	return(Result);
}

int main(void)
{
	int Length = 0;
	char Buffer[BUFFERLENGTH];
	while((Length = GetLine(Buffer, ArrayCount(Buffer))) > 0)
	{
		long Result = HToI(Buffer);
		printf("=> %ld\n", Result);
	}

	return 0;
}
