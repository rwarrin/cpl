#include <stdio.h>

int
BitCount(unsigned int Value)
{
	int Result = 0;

	while(Value != 0)
	{
		++Result;
		Value &= (Value - 1);
	}

	return(Result);
}

int main(void)
{
	int Result = BitCount(0x73);
	printf("%d\n", Result);

	return 0;
}

/*
 * 0111 0011
 * -1
 * 0111 0010
 * 0111 0010
 * -1
 * 0111 0001
 * 0111 0000
 * -1
 * 0110 1111
 * 0110 0000
 * -1
 * 0101 1111
 * 0100 0000
 * -1
 * 0011 1111
 * 0000 0000
 *
 * Result = 5
 */
