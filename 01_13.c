#include <stdio.h>

#define MAX_LENGTHS 50
#define MAX_HEIGHT 10

int main(void)
{
	int Character = 0;
	int Length = 0;
	int Lengths[MAX_LENGTHS];

	for(int i = 0; i < MAX_LENGTHS; ++i)
	{
		Lengths[i] = 0;
	}

	while((Character = getchar()) != EOF)
	{
		if((Character == '\n') ||
		   (Character == '\t') ||
		   (Character == ' '))
		{
			++Lengths[Length];
			Length = 0;
		}
		else
		{
			++Length;
		}
	}

	int HighestFrequency = 0;
	for(int i = 0; i < MAX_LENGTHS; ++i)
	{
		if(Lengths[i] > HighestFrequency)
		{
			HighestFrequency = Lengths[i];
		}
	}

	printf("\n");
	for(int Y = MAX_HEIGHT; Y > 0; --Y)
	{
		for(int X = 1; X < MAX_LENGTHS; ++X)
		{
			float Percent = (float)Lengths[X] / (float)HighestFrequency;
			float Height = (float)MAX_HEIGHT * Percent;
			if(Height >= Y)
			{
				printf("%3c", '*');
			}
			else
			{
				printf("%3c", ' ');
			}
		}
		printf("\n");
	}

	for(int i = 0; i < MAX_LENGTHS; ++i)
	{
		printf("%3d", i);
	}
	printf("\n");

	return 0;
}
