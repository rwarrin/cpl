#include <stdio.h>
#include <limits.h>

int main(void)
{
	printf("signed char min: %d\n", CHAR_MIN);
	printf("signed char max: %d\n", CHAR_MAX);
	printf("unsigned char min: %d\n", 0);
	printf("unsigned char max: %u\n\n", UCHAR_MAX);


	printf("signed short min: %d\n", SHRT_MIN);
	printf("signed short max: %d\n", SHRT_MAX);
	printf("unsigned short min: %d\n", 0);
	printf("unsigned short max: %u\n\n", USHRT_MAX);

	printf("signed int min: %d\n", INT_MIN);
	printf("signed int max: %d\n", INT_MAX);
	printf("unsigned int min: %d\n", 0);
	printf("unsigned int max: %u\n\n", UINT_MAX);

	printf("signed long min: %ld\n", LONG_MIN);
	printf("signed long max: %ld\n", LONG_MAX);
	printf("unsigned long min: %d\n", 0);
	printf("unsigned long max: %lu\n\n", ULONG_MAX);

	printf("signed long long min: %lld\n", LLONG_MIN);
	printf("signed long long max: %lld\n", LLONG_MAX);
	printf("unsigned long long min: %d\n", 0);
	printf("unsigned long long max: %llu\n\n", ULLONG_MAX);

	return 0;
}
