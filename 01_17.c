#include <stdio.h>

#define BUFFERSIZE 85
#define LINELIMIT 80

int GetLine(char Buffer[], int Limit)
{
	int Length = 0;
	int Character = 0;
	for(Length = 0;
		((Length < Limit-1) &&
		(((Character = getchar()) != EOF) ||
		(Character != '\n')));
		 ++Length)
	{
		Buffer[Length] = Character;
	}

	return(Length);
}

int main(void)
{

	char Buffer[BUFFERSIZE] = {0};

	int Length = 0;
	while((Length = GetLine(Buffer, BUFFERSIZE)) != 0)
	{
		if(Length >= LINELIMIT)
		{
			printf("%s", Buffer);
			int Character = 0;
			while((Character = getchar()) != EOF)
			{
				if((Character == '\n') ||
				   (Character == '\0'))
				{
					break;
				}

				putchar(Character);
			}
		}
	}

	return 0;
}
