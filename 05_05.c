#include <stdio.h>

static char *
StringNCopy(char *A, char *B, int Limit)
{
	int Count = 0;
	char *ACopy = A;
	while(((*ACopy++ = *B++) != 0) &&
		  (Count++ < Limit));

	return A;
}

static char *
StringNCat(char *A, char *B, int Limit)
{
	char *AEnd = A;
	while(*AEnd++ != 0);
	--AEnd;

	int Count = 0;
	while(((*AEnd++ = *B++) != 0) &&
		  (Count++ < Limit));
	*(--AEnd) = 0;

	return A;
}

static int
StringNCmp(char *A, char *B, int Limit)
{
	for(int Count = 0;
		((*A++ == *B++) &&
		 (Count < Limit));
		++Count)
	{
		if(*A == 0)
			return 0;
	}

	return *A - *B;
}

int main(void)
{

	char CompareA[100] = "This is the key";
	char CompareB[100] = "This is the key";
	char CompareC[100] = "This is not the key";
	printf("Compare Equal: %d\n", StringNCmp(CompareA, CompareB, 10));
	printf("Compare Unequal: %d\n", StringNCmp(CompareA, CompareC, 10));

	char CatA[100] = "Hello ";
	char CatB[100] = "World! Really long string";
	printf("Cat Short: %s\n", StringNCat(CatA, CatB, 10));
	char CatC[100] = "Hello ";
	char CatD[100] = "World! Really long string";
	printf("Cat Long: %s\n", StringNCat(CatC, CatD, 100));

	char CopyA[100] = "This is a long string to copy into";
	char CopyB[100] = "This is a really long string to copy from";
	printf("Copy Short: %s\n", StringNCopy(CopyA, CopyB, 10));
	char CopyC[100] = "Goodbye World!";
	char CopyD[100] = "Hello  ";
	printf("Copy Full: %s\n", StringNCopy(CopyC, CopyD, 100));

	return 0;
}
