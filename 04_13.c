#include <stdio.h>

int
StringLength(char String[])
{
	int Length = 0;
	for(Length = 0; String[Length] != 0; ++Length);

	return(Length);
}

static void
Swap(char String[], int Left, int Right)
{
	char Temp = String[Left];
	String[Left] = String[Right];
	String[Right] = Temp;
}

static void
ReverseR(char String[], int Left, int Right)
{
	if(Left >= Right)
	{
		// Stop recursing once we've reached the middle of the string
		return;
	}
	else
	{
		ReverseR(String, Left + 1, Right - 1);
	}

	Swap(String, Left, Right);
}

void
Reverse(char String[])
{
	int Length = StringLength(String);
	ReverseR(String, 0, Length - 1);
}

int main(void)
{
	char String[] = "Some String To Reverse!";
	printf("%s\n", String);

	Reverse(String);
	printf("%s\n", String);

	return 0;
}
