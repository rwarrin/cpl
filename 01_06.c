#include <stdio.h>

int main(void)
{
	int value = getchar() != EOF;
	printf("%d\n", value);

	return 0;
}
