#include <stdio.h>

#define BUFFERSIZE 1000

int
IsValidCharacter(char Character)
{
	if(((Character >= 'a') && (Character <= 'z')) ||
	   ((Character >= 'A') && (Character <= 'Z')) ||
	   ((Character >= '0') && (Character <= '9')))
	{
		return 1;
	}

	return 0;
}

int
Expand(char In[], char Out[])
{
	// a-d in
	// abcd out
	int InIndex = 0;
	int OutIndex = 0;

	while(In[InIndex] == '-')
	{
		Out[OutIndex++] = In[InIndex];
		++InIndex;
	}

	char LastCharacter = 0;
	for(; In[InIndex] != 0; ++InIndex)
	{
		char Character = In[InIndex];

		if(Character == '-')
		{
			int NewInIndex = InIndex;
			while(!IsValidCharacter(In[NewInIndex]))
			{
				if(In[NewInIndex] == 0)
				{
					break;
				}
				++NewInIndex;
			}

			Character = In[NewInIndex];
			if(Character == 0)
			{
				Out[OutIndex++] = In[InIndex];
			}
			else if(IsValidCharacter(Character))
			{
				for(int i = LastCharacter+1; i <= Character; ++i)
				{
					Out[OutIndex++] = (char)i;
				}
				LastCharacter = Character;
				InIndex = NewInIndex;
			}
		}
		else if(IsValidCharacter(Character))
		{
			LastCharacter = Character;
			Out[OutIndex++] = Character;
		}
	}

	Out[OutIndex++] = 0;

	return OutIndex;
}

int main(void)
{
	char Input[] = "--ab--g-mx---z--12-5-8--";
	char Output[BUFFERSIZE];

	int OutputLength = Expand(Input, Output);
	printf("%s\n", Output);

	return 0;
}
