#include <stdio.h>

int main(void)
{
	double WhitespaceCount = 0;
	int character = 0;

	while((character = getchar()) != EOF)
	{
		if((character == '\n') ||
		   (character == '\t') ||
		   (character == ' '))
		{
			++WhitespaceCount;
		}
	}

	printf("Whitespace characters found: %.0f\n", WhitespaceCount);

	return 0;
}
