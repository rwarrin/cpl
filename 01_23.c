#include <stdio.h>

/* This is a muliline comment on a single line */

int main(void)
{
	// NOTE(rick): This is a single line comment
	int Character = 0;

	while((Character = getchar()) != EOF) // NOTE(rick): End of line comment
	{
		if(Character == '/')
		{
			Character = getchar();
			if(Character == '/')
			{
				while((Character = getchar()) != '\n');
				putchar('\n');
			}
			else if(Character == '*')
			{
				while((Character = getchar()))
				{
					if(Character == '*')
					{
						Character = getchar();
						if(Character == '/')
						{
							break;
						}
					}
				}
			}
			else
			{
				printf("/%c", Character);
			}
		}
		else
		{
			putchar(Character);
		}
	}

	/*
	 * This is a multiline comment on
	 * multiple lines
	 */

	return 0;
}
