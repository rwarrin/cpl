#include <stdio.h>
#include <limits.h>

#define MAXBUFFERSIZE 1000

static void
ZeroMemory(void *Memory, int Size)
{
	unsigned char *Ptr = Memory;
	while(Size--)
	{
		*Ptr++ = 0;
	}
}

static int
StringLength(char *String)
{
	int Length = 0;
	while(*String++ != 0)
	{
		++Length;
	}

	return(Length);
}

static int
GetLine(char *Buffer, int Limit)
{
	int Count = 0;
	int Character = 0;
	while(((*Buffer++ = Character = getchar()) != EOF) &&
		  (Character != '\n') &&
		  (Count++ < Limit));

	*Buffer = 0;

	return(Count);
}

// NOTE(rick): Assumes Buffer has enough storage to store Value
static void
itoa(int Value, char *Buffer)
{
	static int Depth = 0;
	if(Value == 0)
	{
		return;
	}

	++Depth;
	itoa(Value / 10, Buffer);
	--Depth;

	int IsNegative = 0;
	if((Value < 0))
	{
		IsNegative = 1;
	}

	int Digit = (IsNegative ? -(Value % 10) : (Value % 10));
	int Character = Digit + '0';
	int End = StringLength(Buffer);
	*(Buffer + End) = Character;

	if((IsNegative) && (Depth == 0))
	{
		for(int Length = StringLength(Buffer);
			Length >= 0;
			--Length)
		{
			*(Buffer + Length) = *(Buffer + Length - 1);
		}

		*Buffer = '-';
	}
}

static int
atoi(char *Input)
{
	int Result = 0;
	int Negative = (*Input == '-');
	if((*Input == '-') || (*Input == '+'))
	{
		++Input;
	}

	int Character = 0;
	while((Character = *Input++) != 0)
	{
		Result = Result*10 + (Character - '0');
	}

	if(Negative)
	{
		Result = -Result;
	}

	return(Result);
}

int main(void)
{

#if 0
	char Buffer[MAXBUFFERSIZE];
	int Count = 0;
	while((Count = GetLine(Buffer, MAXBUFFERSIZE)) > 0)
	{
		printf("(%d)%s", Count, Buffer);
	}
#endif

#if 1
	char NumberString[MAXBUFFERSIZE] = {0};
	ZeroMemory(NumberString, MAXBUFFERSIZE);
	int Number = INT_MIN;
	itoa(Number, NumberString);
	printf("%d = %s\n", Number, NumberString);

	Number = INT_MAX;
	ZeroMemory(NumberString, MAXBUFFERSIZE);
	itoa(Number, NumberString);
	printf("%d = %s\n", Number, NumberString);
#endif

#if 0
	char *Numbers = "12345";
	int GotNumbers = atoi(Numbers);
	printf("%d\n", GotNumbers);
#endif

	return 0;
}
