#include <stdio.h>
#include <stdlib.h>


typedef int bool32;
#define true 1
#define false 0

#define ArrayCount(Array) (sizeof(Array) / (sizeof(Array)[0]))

#define Assert(Expression) if(!(Expression)) { *(int *)0 = 0; }

#define InvalidCodePath Assert(!"Unreachable Code")
#define InvalidDefaultCase default: { InvalidCodePath; } break;

#define DOC_PARAM(name, description)
#define DOC_RETURN(type, description)
// TODO(rick): I need a way to ignore the description define otherwise it will
// show up in the generated docs.
#define DOC_DESCRIPTION(description)

struct file_result
{
	char *FileContents;
	unsigned int FileSize;
};

enum token_type
{
	TokenType_EndOfStream,
	TokenType_Unknown,

	TokenType_Identifier,

	TokenType_SemiColon,
	TokenType_Colon,
	TokenType_Asterisk,
	TokenType_Dot,
	TokenType_Comma,
	TokenType_OpenParen,
	TokenType_CloseParen,
	TokenType_OpenBracket,
	TokenType_CloseBracket,
	TokenType_OpenBrace,
	TokenType_CloseBrace,
	TokenType_Character,
	TokenType_String,

	TokenType_Comment,
	TokenType_BlockComment,

	TokeType_PreProcessorDirective,

	TokenType_Count,
};

struct token
{
	enum token_type Type;
	size_t Length;
	char *Text;
};

struct tokenizer
{
	char *At;
};

static int
StringLength(char *String)
{
	int Result = 0;

	while(*String++ != 0)
	{
		++Result;
	}

	return(Result);
}

DOC_DESCRIPTION(Determines whether a character is a whitespace character.);
DOC_PARAM(Character, The character to check.);
DOC_RETURN(bool32, Returns true if the character is whitespace.);
static bool32
IsWhitespace(char Character)
{
	bool32 Result = ((Character == ' ') ||
					 (Character == '\t'));
	return(Result);
}

DOC_DESCRIPTION(Determines whether a character is a newline character.);
DOC_PARAM(Character, The character to check.);
DOC_RETURN(bool32, Returns true if the character is a newline.);
static bool32
IsNewLine(char Character)
{
	bool32 Result = ( (Character == '\n') ||
					  (Character == '\r') );
	return(Result);
}

DOC_DESCRIPTION(Determines whether a character is an alphabet character.);
DOC_PARAM(Character, The character to check.);
DOC_RETURN(bool32, Returns true if the character is an alphabet character.);
static bool32
IsAlpha(char Character)
{
	bool32 Result = (( (Character >= 'a') && (Character <= 'z') ) ||
					 ( (Character >= 'A') && (Character <= 'Z') ));
	return(Result);
}

DOC_DESCRIPTION(Determines whether a character is a number.);
DOC_PARAM(Character, The character to check.);
DOC_RETURN(bool32, Returns true if the character is a number.);
static bool32
IsNumeric(char Character)
{
	bool32 Result = ( (Character >= '0') && (Character <= '9') );
	return(Result);
}

DOC_DESCRIPTION(Determines whether a character is a special character.);
DOC_PARAM(Character, The character to check.);
DOC_RETURN(bool32, Returns true if the character is a special character.);
static bool32
IsSpecial(char Character)
{
	bool32 Result = ( (Character == '_') );
	return(Result);
}

DOC_DESCRIPTION(Opens the specified file and copies the entire contents into memory.);
DOC_PARAM(FileName, The name of the file to open and read into memory.);
DOC_RETURN(struct file_result, Returns a file_result which contains the size and contents of the file.);
static struct file_result
ReadEntireFileIntoMemory(char *FileName)
{
	struct file_result Result = {0};

	FILE *File = fopen(FileName, "rb");
	// NOTE(rick): We need to set the type of IO buffering used on this file
	// stream to unbuffered because we ran into an issue where we tried to close
	// the file before the buffered read was pushed out to the application which
	// resulted in a seg-fault. The buffering type must be set before any
	// operations are done to the file stream.
	setvbuf(File, NULL, _IONBF, 0);

	fseek(File, 0, SEEK_END);
	Result.FileSize = ftell(File);
	fseek(File, 0, SEEK_SET);

	Result.FileContents = (char *)malloc(Result.FileSize);
	if(Result.FileContents)
	{
		fread(Result.FileContents, Result.FileSize, 1, File);
		Result.FileContents[Result.FileSize - 1] = 0;  // NULL terminate
	}
	else
	{
		Result.FileSize = 0;
	}

	fclose(File);
	return(Result);
}

DOC_DESCRIPTION(Frees memory allocated to a file_result struct object.);
DOC_PARAM(struct file_result, The file_result object to release.);
static void
RemoveFileFromMemory(struct file_result *File)
{
	if(File->FileContents)
	{
		free(File->FileContents);
		File->FileContents = NULL;
	}

	File->FileSize = 0;
}

DOC_DESCRIPTION(Moves the tokenizer to the next non-whitespace character.);
DOC_PARAM(struct tokenizer *Tokenizer, Tokenizer to remove whitespace from.);
static void
EatWhitespace(struct tokenizer *Tokenizer)
{
	while(IsWhitespace(*Tokenizer->At) ||
		  IsNewLine(*Tokenizer->At))
	{
		++Tokenizer->At;
	}
}

DOC_DESCRIPTION(Gets a token from the tokenizer.);
DOC_PARAM(struct tokenizer *Tokenzier, The tokenizer to get a token from.);
DOC_RETURN(struct token, Returns a struct token.);
static struct token
GetToken(struct tokenizer *Tokenizer)
{
	EatWhitespace(Tokenizer);

	struct token Token = {0};
	Token.Length = 1;
	Token.Text = Tokenizer->At;
	Token.Type = TokenType_Unknown;
	
	char Character = *(Tokenizer->At);
	++Tokenizer->At;
	switch(Character)
	{
		case '\0': { Token.Type = TokenType_EndOfStream; } break;
		case ';':  { Token.Type = TokenType_SemiColon; } break;
		case ':':  { Token.Type = TokenType_Colon; } break;
		case '*':  { Token.Type = TokenType_Asterisk; } break;
		case '.':  { Token.Type = TokenType_Dot; } break;
		case ',':  { Token.Type = TokenType_Comma; } break;
		case '(':  { Token.Type = TokenType_OpenParen; } break;
		case ')':  { Token.Type = TokenType_CloseParen; } break;
		case '[':  { Token.Type = TokenType_OpenBracket; } break;
		case ']':  { Token.Type = TokenType_CloseBracket; } break;
		case '{':  { Token.Type = TokenType_OpenBrace; } break;
		case '}':  { Token.Type = TokenType_CloseBrace; } break;

		// TODO(rick): This is just temporary while I work through the
		// documentation parsing. I need this because the #define's are being
		// parsed by the doc parser. Can probably solve this by simply moving
		// the #define's to another file, but I think a more robust solution
		// would be better.
		case '#':
		{
			Token.Type = TokeType_PreProcessorDirective;
			while(!(IsNewLine(*Tokenizer->At)))
			{
				++Tokenizer->At;
			}

			Token.Length = Tokenizer->At - Token.Text;
		} break;

		case '\'':
		{
			Token.Type = TokenType_Character;
			Token.Text = Tokenizer->At;
			while(*Tokenizer->At && 
				  (*Tokenizer->At != '\''))
			{
				if((*Tokenizer->At == '\\') &&
				   (*(Tokenizer->At + 1)))
				{
					++Tokenizer->At;
				}
				++Tokenizer->At;
			}

			Token.Length = Tokenizer->At - Token.Text;
			if(*Tokenizer->At == '\'')
			{
				++Tokenizer->At;
			}
		} break;

		case '"':
		{
			Token.Type = TokenType_String;
			Token.Text = Tokenizer->At;

			while(*Tokenizer->At &&
				  (*Tokenizer->At != '"'))
			{
				if((*Tokenizer->At == '\\') &&
				   (*(Tokenizer->At + 1)))
				{
					++Tokenizer->At;
				}

				++Tokenizer->At;
			}

			Token.Length = Tokenizer->At - Token.Text;
			if(*Tokenizer->At == '"')
			{
				++Tokenizer->At;
			}
		} break;

		case '/':
		{
			if(*Tokenizer->At == '/')
			{
				Token.Text = Token.Text + 2;
				Token.Type = TokenType_Comment;
				while(!IsNewLine(*Tokenizer->At))
				{
					++Tokenizer->At;
				}

				Token.Length = Tokenizer->At - Token.Text;
			}
			else if(*Tokenizer->At == '*')
			{
				/* This parses block comments
				 * they can span multiple lines
				 */
				Token.Text = Token.Text + 2;
				Token.Type = TokenType_BlockComment;
				while(*Tokenizer->At)
				{
					if(*Tokenizer->At == '*')
					{
						if((*(Tokenizer->At + 1)) &&
						   (*(Tokenizer->At + 1) == '/'))
						{
							Token.Length = Tokenizer->At - Token.Text;
							++Tokenizer->At;
							break;
						}
					}

					++Tokenizer->At;
				}
			}
		} break;

		default:
		{
			Token.Type = TokenType_Identifier;

			while((*Tokenizer->At != 0) &&
				  ((IsAlpha(*Tokenizer->At)) ||
				  (IsSpecial(*Tokenizer->At)) ||
				  (IsNumeric(*Tokenizer->At))))
			{
				++Tokenizer->At;
			}

			Token.Length = Tokenizer->At - Token.Text;
		} break;
	}

	return(Token);
}

static bool32
TokenMatches(struct token Token, char *Match)
{
	char *TextPtr = Token.Text;

	while((*Match != 0) && (*Match == *TextPtr))
	{
		++Match, ++TextPtr;
	}

	bool32 Result = (*Match == 0);
	return(Result);
}

// NOTE(rick): TokenOut can be used to save the next token retrieved from the
// tokenizer. This value can be NULL.
static bool32
RequireToken(struct tokenizer *Tokenizer, enum token_type RequiredType,
			 struct token *TokenOut)
{
	struct token Token = {0};
	if(TokenOut == NULL)
	{
		TokenOut = &Token;
	}

	*TokenOut = GetToken(Tokenizer);
	bool32 Result = (TokenOut->Type == RequiredType);
	return(Result);
}

// NOTE(rick): TokenOut is used to get the last token read back from this
// function. This parameter can be NULL if you don't need to know what it was.
static void
ConsumeTokensUntilType(struct tokenizer *Tokenizer, enum token_type TokenType,
					   struct token *TokenOut)
{
	struct token Token = {0};

	while(((Token = GetToken(Tokenizer)).Type != TokenType) &&
		  (Token.Type != TokenType_EndOfStream));

	if(TokenOut != 0)
	{
		*TokenOut = Token;
	}
}

static void
PutTokenBack(struct tokenizer *Tokenizer, struct token Token)
{
	Tokenizer->At -= Token.Length;
}

static void
ParseFunctionDescription(struct tokenizer *Tokenizer)
{
	struct token Token = {0};
	if(RequireToken(Tokenizer, TokenType_OpenParen, &Token))
	{
		printf("DOC_DESCRIPTION: ");
		while((Token = GetToken(Tokenizer)).Type != TokenType_CloseParen)
		{
			printf("%.*s ", (int)Token.Length, Token.Text);
		}
		printf("\n");
	}
	else
	{
		printf("Not a valid document description.\n");
	}
}

static void
ParseFunctionParameter(struct tokenizer *Tokenizer)
{
	struct token Token = {0};
	if(RequireToken(Tokenizer, TokenType_OpenParen, &Token))
	{
		printf("DOC_PARAMETER: ");
		while((Token = GetToken(Tokenizer)).Type != TokenType_CloseParen)
		{
			printf("%.*s ", (int)Token.Length, Token.Text);
		}
		printf("\n");
	}
	else
	{
		printf("Not a valid document parameter.\n");
	}
}

static void
ParseFunctionReturn(struct tokenizer *Tokenizer)
{
	struct token Token = {0};
	if(RequireToken(Tokenizer, TokenType_OpenParen, &Token))
	{
		printf("DOC_RETURN: ");
		while((Token = GetToken(Tokenizer)).Type != TokenType_CloseParen)
		{
			printf("%.*s ", (int)Token.Length, Token.Text);
		}
		printf("\n");
	}
	else
	{
		printf("Not a valid document return.\n");
	}
}

static void
ParseFunctionDefinition(struct tokenizer *Tokenizer)
{
	struct token Token = {0};
	printf("DEFINITION: ");
	while((Token = GetToken(Tokenizer)).Type != TokenType_CloseParen)
	{
		printf("%.*s ", (int)Token.Length, Token.Text);
	}
	printf("%.*s\n", (int)Token.Length, Token.Text);
}

static void
GetDocumentationItem(struct tokenizer *Tokenizer, char **Location, int *Length)
{
	*Location = 0;

	struct token Token = {0};
	if(RequireToken(Tokenizer, TokenType_OpenParen, &Token))
	{
		*Location = Tokenizer->At;
		while((Token = GetToken(Tokenizer)).Type != TokenType_CloseParen);
		PutTokenBack(Tokenizer, Token);
		*Length = Tokenizer->At - *Location;
	}
}

static void
GetSignatureForDocumentation(struct tokenizer *Tokenizer, char **Location, int *Length)
{
	*Location = Tokenizer->At;

	struct token Token = {0};
	while((Token = GetToken(Tokenizer)).Type != TokenType_OpenBrace);

	PutTokenBack(Tokenizer, Token);
	*Length = Tokenizer->At - *Location;
}

DOC_DESCRIPTION(Parses documentation for a function.);
DOC_PARAM(struct tokenizer *Tokenizer, The tokenizer.);
DOC_RETURN(struct token, Returns the last token read after parsing all documentation.);
static struct token
ParseFunctionDocumentation(struct tokenizer *Tokenizer)
{
	//struct documentation_item DocItem = {0};
	ParseFunctionDescription(Tokenizer);

	struct token Token = {0};
	ConsumeTokensUntilType(Tokenizer, TokenType_Identifier, &Token);
	while(TokenMatches(Token, "DOC_PARAM"))
	{
		ParseFunctionParameter(Tokenizer);
		ConsumeTokensUntilType(Tokenizer, TokenType_Identifier, &Token);
	}

	if(TokenMatches(Token, "DOC_RETURN"))
	{
		ParseFunctionReturn(Tokenizer);
	}

	ConsumeTokensUntilType(Tokenizer, TokenType_Identifier, &Token);
	PutTokenBack(Tokenizer, Token);
	ParseFunctionDefinition(Tokenizer);

	return(Token);
}

#include "06_01-writer.c"

static struct documentation_item
GetDocumentation(struct tokenizer *Tokenizer)
{
	struct documentation_item Result = {0};
	char *DocItemLocation = 0;
	int DocItemLength = 0;
	struct token Token = {0};

	Token = GetToken(Tokenizer);
	if(TokenMatches(Token, "DOC_DESCRIPTION"))
	{
		GetDocumentationItem(Tokenizer, &DocItemLocation, &DocItemLength);
		Result.Description = DocItemLocation;
		Result.DescriptionLength = DocItemLength;
		ConsumeTokensUntilType(Tokenizer, TokenType_Identifier, &Token);
	}

	while(TokenMatches(Token, "DOC_PARAM"))
	{
		DocItemLocation = 0;
		DocItemLength = 0;
		GetDocumentationItem(Tokenizer, &DocItemLocation, &DocItemLength);

		int Index = Result.ParamsCount;
		Result.Params[Index] = DocItemLocation;
		Result.ParamsLength[Index] = DocItemLength;
		++Result.ParamsCount;

		ConsumeTokensUntilType(Tokenizer, TokenType_Identifier, &Token);
	}

	if(TokenMatches(Token, "DOC_RETURN"))
	{
		DocItemLocation = 0;
		DocItemLength = 0;
		GetDocumentationItem(Tokenizer, &DocItemLocation, &DocItemLength);

		Result.Return = DocItemLocation;
		Result.ReturnLength = DocItemLength;

		ConsumeTokensUntilType(Tokenizer, TokenType_Identifier, &Token);
	}

	DocItemLocation = 0;
	DocItemLength = 0;
	PutTokenBack(Tokenizer, Token);
	char *SignatureStart = Tokenizer->At;

	GetSignatureForDocumentation(Tokenizer, &DocItemLocation, &DocItemLength);
	Result.Signature = DocItemLocation;
	Result.SignatureLength = DocItemLength;

	Tokenizer->At = SignatureStart;
	struct token TempToken = {0};
	while((TempToken = GetToken(Tokenizer)).Type != TokenType_OpenParen)
	{
		Token = TempToken;
	}
	Result.Name = Token.Text;
	Result.NameLength = Token.Length;

	return(Result);
}


DOC_DESCRIPTION(Main function)
DOC_PARAM(Argc, Number of command line arguments supplied.);
DOC_PARAM(Argv, Pointer to an array of pointers to CStrings with NULL terminated command line arguments.);
DOC_RETURN(int, status);
int
main(int Argc, char **Argv)
{
	if(Argc < 2)
	{
		fprintf(stderr, "Usage: %s [file]\n", Argv[0]);
		return 1;
	}

	struct file_result File = ReadEntireFileIntoMemory(*(Argv + 1));

	struct tokenizer Tokenizer = {0};
	Tokenizer.At = File.FileContents;

	struct documentation_writer DocWriter = Documentation_Init();
	Documentation_SetPageTemplate(&DocWriter, "06_01-page_template.html");
	Documentation_SetDocTemplate(&DocWriter, "06_01-doc_template.html");

	bool32 Parsing = true;
	while(Parsing)
	{
		struct token Token = GetToken(&Tokenizer);
		switch(Token.Type)
		{
			case TokenType_EndOfStream:
			{
				Parsing = false;
			} break;
			case TokenType_Identifier:
			{
				//printf("IDENTIFIER(%.*s)\n", (int)Token.Length, Token.Text);
#if 1
				if(TokenMatches(Token, "DOC_"))
				{
					PutTokenBack(&Tokenizer, Token);
					struct documentation_item Item = GetDocumentation(&Tokenizer);
					DocWriter.DocItems[DocWriter.DocItemsCount++] = Item;
				}
#endif
			} break;
			case TokenType_String:
			{
				//printf("STRING(%.*s)\n", (int)Token.Length, Token.Text);
			} break;
			case TokenType_Character:
			{
				//printf("CHARACTER(%.*s)\n", (int)Token.Length, Token.Text);
			} break;
			case TokenType_Comment:
			{
				//printf("COMMENT(%.*s)\n", (int)Token.Length, Token.Text);
			} break;
			case TokenType_BlockComment:
			{
				//printf("BLOCK_COMMENT(%.*s)\n", (int)Token.Length, Token.Text);
			} break;
			case TokenType_Unknown:
			{
				//printf("UNKNOWN(%.*s)\n", (int)Token.Length, Token.Text);
			} break;
			default:
			{
			} break;
		}
	}

	if(DocWriter.DocItemsCount > 0)
	{
		for(int DocItemIndex = 0;
			DocItemIndex < DocWriter.DocItemsCount;
			++DocItemIndex)
		{
			struct documentation_item *Item = DocWriter.DocItems + DocItemIndex;
			printf("%.*s\n", Item->DescriptionLength, Item->Description);
			for(int ParamIndex = 0;
				ParamIndex < Item->ParamsCount;
				++ParamIndex)
			{
				printf("%.*s\n", Item->ParamsLength[ParamIndex], Item->Params[ParamIndex]);
			}
			printf("%.*s\n", Item->ReturnLength, Item->Return);
			printf("%.*s\n", Item->SignatureLength, Item->Signature);
		}
	}

	if(DocWriter.TokensCount)
	{
		for(int TokensIndex = 0;
			TokensIndex < DocWriter.TokensCount;
			++TokensIndex)
		{
			struct replaceable_tokens *Tokens = DocWriter.Tokens + TokensIndex;
			if(Tokens->TokenCount > 0)
			{
				for(int Index = 0;
					Index < Tokens->TokenCount;
					++Index)
				{
					struct replaceable_token *Token = (Tokens->Tokens + Index);
					printf("(%p, %p) %.*s\n", Token->Start, Token->End, Token->Length, Token->Start);
				}
			}
		}
	}

	Documentation_Write(&DocWriter, "06_01-docs.html");
	Documentation_End(&DocWriter);
	RemoveFileFromMemory(&File);
	return 0;
}
