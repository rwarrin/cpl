#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int
main(void)
{
	char **Strings;
	Strings = (char **)malloc(sizeof(char *) * 2);

	char *Test1 = (char *)malloc(sizeof(char) * 10);
	strcpy(Test1, "Hello");
	char *Test2 = (char *)malloc(sizeof(char) * 10);
	strcpy(Test2, "World");

	*(Strings + 0) = Test1;
	*(Strings + 1) = Test2;
	printf("%s\n", *Strings);
	printf("%s\n", *(Strings + 1));

	*(*(Strings + 0) + 3) = 'p';
	*(*(Strings + 0) + 4) = 0;
	printf("%s\n", *Strings);
	printf("%s\n", *(Strings + 1));

	free(*Strings);
	free(*(Strings + 1));
	free(Strings);

	return 0;
}
