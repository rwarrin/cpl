#include <stdio.h>
#include <stdlib.h>

#define PAGESIZE 10
#define MAXLINE 1000

int
main(int Argc, char **Argv)
{
	if(Argc < 2)
	{
		printf("Usage: %s [filenames]\n", Argv[0]);
		return 1;
	}

	char ReadLine[MAXLINE] = {0};
	for(int ArgIndex = 1;
		ArgIndex < Argc;
		++ArgIndex)
	{
		char *Filename = Argv[ArgIndex];
		FILE *File = fopen(Filename, "rb");
		if(File)
		{
			int PageCount = 0;
			int LineCount = 0;
			printf("[%-20s] page: %-10d\n", Filename, PageCount);

			while(fgets(ReadLine, MAXLINE, File) != 0)
			{
				if(LineCount >= PAGESIZE)
				{
					++PageCount;
					LineCount = 0;
					printf("\n\n\n\n\n[%-20s] page: %-10d\n", Filename, PageCount);
				}

				printf("%s", ReadLine);
				++LineCount;
			}

			fclose(File);
		}
	}

	return 0;
}
