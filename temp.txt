#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>

#ifdef stdout
#undef stdout
#define stdout 1
#endif

#define BUFFER_SIZE 1024

int
main(int Argc, char **Argv)
{
	if(Argc < 2)
	{
		printf("Usage: %s [file name 1..n]\n", Argv[0]);
		return 1;
	}

	char *ReadBuffer = (char *)malloc(sizeof(char) * BUFFER_SIZE);
	int BytesRead = 0;

	for(int ArgIndex = 1;
		ArgIndex < Argc;
		++ArgIndex)
	{
		int FileDescriptor = open(Argv[ArgIndex], O_RDONLY);
		if(FileDescriptor != -1)
		{
			while((BytesRead = read(FileDescriptor, ReadBuffer, BUFFER_SIZE)) > 0)
			{
				write(stdout, ReadBuffer, BytesRead);
			}

			close(FileDescriptor);
		}
	}

	free(ReadBuffer);

	return 0;
}
#include <unistd.h>

int
main(void)
{
	char Character = 0;

	while((read(0, &Character, 1)) == 1)
	{
		write(1, &Character, 1);
	}

	return 0;
}
