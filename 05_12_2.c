/**
 * Implementation of unix program TAIL
 */

#include <stdio.h>
#include <stdlib.h>

static int
StringCompare(char *A, char *B)
{
	while((*A != 0) && (*A == *B))
	{
		++A, ++B;
	}

	return *A - *B;
}

static void
StringCopy(char *Destination, char *Source)
{
	while((*Destination++ = *Source++) != 0);
}

static int
GetLine(char **Destination)
{
#define LINELIMIT 1000

	int Length = 0;
	int Character = 0;
	char Line[LINELIMIT] = {0};

	while(((Line[Length] = Character = getchar()) != EOF) &&
		  (Length++ < LINELIMIT))
	{
		if(Character == '\n')
		{
			Line[Length - 1] = 0;
			break;
		}
	}

	if(Length)
	{
		char *NewString = (char *)malloc(sizeof(char) * Length);
		StringCopy(NewString, Line);
		*Destination = NewString;
	}

#undef LINELIMIT
	return(Length);
}

static void
PrintWriteBack(char **Lines, int Count)
{
	for(int Index = Count - 1; Index >= 0; --Index)
	{
		if(*(Lines + Index) != 0)
		{
			printf("%s\n", *(Lines + Index));
		}
	}
}

int
main(int Argc, char **Argv)
{
	int ReadBackLines = 10;

	while(Argc-- > 1)
	{
		int Character = 0;
		char *Argument = *(++Argv);
		if(*Argument == '-')
		{
			while((Character = *(++Argument)) != 0)
			{
				switch(Character)
				{
					// NOTE(rick): Number of lines to print
					case 'n':
					{
						if((*(Argv + 1)) != 0)
						{
							ReadBackLines = atoi(*(Argv + 1));
						}
					} break;

					default:
					{
						printf("Unknown switch %c\n", Character);
					} break;
				}
			}
		}
	}

	// NOTE(rick): Lines is used as a pointer to an array of pointers to chars.
	char **Lines = (char **)malloc(sizeof(char *) * ReadBackLines);

	int Count = 0;
	char *NewLine = 0;
	while((Count = GetLine(&NewLine)) != 0)
	{
		char *FreeLine = *(Lines + ReadBackLines - 1);
		for(int LinesIndex = ReadBackLines - 1; LinesIndex > 0; --LinesIndex)
		{
			*(Lines + LinesIndex) = *(Lines + LinesIndex -1);
		}
		*(Lines) = NewLine;
		free(FreeLine);
	}

	PrintWriteBack(Lines, ReadBackLines);

	// NOTE(rick): Cleanup memory in use
	for(int LinesIndex = 0; LinesIndex < ReadBackLines; ++LinesIndex)
	{
		if(*(Lines + LinesIndex))
		{
			free(*(Lines + LinesIndex));
		}
	}
	free(Lines);

	return 0;
}
