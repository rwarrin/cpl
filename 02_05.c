#include <stdio.h>
#include <limits.h>

#define ArrayCount(Array) (sizeof(Array) / sizeof((Array)[0]))
#define BUFFERSIZE 1000

int
GetLine(char Buffer[], int Limit)
{
	int Length = 0;
	int Character = 0;

	while(((Character = getchar()) != EOF) &&
		  (Length < Limit - 1))
	{
		Buffer[Length++] = Character;
		if(Character == '\n')
		{
			break;
		}
	}

	if(Length > 0)
	{
		Buffer[Length++] = 0;
	}

	return(Length);
}

int
Any(char Input[], char Find[])
{
	int Index = INT_MAX;

	for(int FindIndex = 0;
		Find[FindIndex] != 0;
		++FindIndex)
	{
		for(int InputIndex = 0;
			Input[InputIndex] != 0;
			++InputIndex)
		{
			if(Input[InputIndex] == Find[FindIndex])
			{
				if(InputIndex < Index)
				{
					Index = InputIndex;
				}

				// NOTE(rick): This is an early out, if we find a character at
				// the first position in the input string then we have found the
				// earliest occurce of any character to find so we can stop.
				if(Index == 0)
				{
					return(Index);
				}
			}
		}
	}

	// NOTE(rick): If no matches are found then replace INT_MAX with -1 which is
	// an easier sentinel value to check against since -1 could never be a valid
	// index in an array.
	if(Index == INT_MAX)
	{
		Index = -1;
	}

	return(Index);
}

int main(void)
{
	int StringLength = 0;
	char String[BUFFERSIZE];
	printf("Input String >");
	StringLength = GetLine(String, ArrayCount(String));

	int FindLength = 0;
	char Find[BUFFERSIZE];
	printf("Find Characters >");
	FindLength = GetLine(Find, ArrayCount(Find));
	if(FindLength)
	{
		Find[FindLength - 2] = 0;
	}

	int Index = Any(String, Find);
	printf("%d\n", Index);

	return 0;
}
