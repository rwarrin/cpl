#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAXLINES 5000

char *lineptr[MAXLINES];

#define MAXLEN 1000

static int
StringCopy(char *Buffer, char *Source)
{
	int Length = 0;
	while((*Buffer++ = *Source++) != 0)
	{
		++Length;
	}
	
	return(Length);
}

static int
StringCompare(char *A, char *B)
{
	printf("Comparing\n\t%s\n\t%s\n", A, B);

	while((*A != 0) && (*A == *B))
	{
		++A, ++B;
	}

	return(*A - *B);
}

static int
GetLine(char *Buffer, int Limit)
{
	int Count = 0;
	char *BufferStart = Buffer;
	int Character = 0;

	while(((*Buffer++ = Character = getchar()) != EOF) &&
		  (Count++ < Limit))
	{
		if(Character == '\n')
		{
			break;
		}
	}

	return(Count);
}

static int
ReadLines(char *lineptr[], int Limit)
{
	int LinesRead = 0;
	int Length = 0;
	char *Memory = 0;
	char Line[MAXLEN] = {0};

	while((Length = GetLine(Line, MAXLEN)) > 0)
	{
		if((LinesRead < Limit) && ((Memory = malloc(Length)) != 0))
		{
			Line[Length - 1] = 0;
			StringCopy(Memory, Line);
			lineptr[LinesRead++] = Memory;
			printf("(%d) %s\n", Length, Line);
		}
		else
		{
			return(-1);
		}
	}

	return(LinesRead);
}

static void
WriteLines(char *lineptr[], int Lines)
{
	for(int Index = 0;
		Lines >= 0;
		--Lines, ++Index)
	{
		printf("%s\n", lineptr[Index]);
	}
}

static void
Swap(char **A, char **B)
{
	printf("Swapping\n\t%s\n\t%s\n", *A, *B);
	char *Temp = *A;
	*A = *B;
	*B = Temp;
}

static void
QuickSort(char *LinePtr[], int Left, int Right)
{
	if(Left >= Right)
	{
		return;
	}

	Swap(LinePtr + Left, LinePtr + ((Left + Right) / 2));

	int LeftTrack = Left;
	for(int Index = Left + 1; Index <= Right; ++Index)
	{
		if(StringCompare(*(LinePtr + Index), *(LinePtr + Left)) < 0)
		{
			++LeftTrack;
			Swap(LinePtr + LeftTrack, LinePtr + Index);
		}
	}
	Swap(LinePtr + Left, LinePtr + LeftTrack);
	
	QuickSort(LinePtr, Left, LeftTrack - 1);
	QuickSort(LinePtr, Left + 1, Right);
}

int main(void)
{
	int Lines;

	if((Lines = ReadLines(lineptr, MAXLINES)) >= 0)
	{
		QuickSort(lineptr, 0, Lines - 1);
		WriteLines(lineptr, Lines - 1);
	}
	else
	{
		fprintf(stderr, "Error: input too big to sort\n");
		return 1;
	}

	return 0;
}
