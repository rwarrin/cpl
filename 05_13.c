#include <stdio.h>
#include <stdlib.h>

#define global_variable static
#define internal static
#define CONFIG_VAR static

#define Assert(Expresssion) if(!(Expression)) { *(int *)0 = 0; }

enum SortDirection
{
	SortDirection_Descending = -1,
	SortDirection_Ascending = 1,
};

CONFIG_VAR enum SortDirection Config_Direction = SortDirection_Ascending;
CONFIG_VAR unsigned int Config_MaxLines = 5000;
CONFIG_VAR unsigned int Config_ShowSettings = 0;

internal void
PrintConfig()
{
	printf("Max Lines: %u\n", Config_MaxLines);
	printf("Sort Direction: %s\n", (Config_Direction == SortDirection_Ascending ? "Ascending" : "Descending"));
}

internal void
ZeroMemory(void *Memory, int Size)
{
	unsigned char *Ptr = Memory;
	while(Size--)
	{
		*Ptr++ = 0;
	}
}

internal void
Swap(void **A, void **B)
{
	void *Temp = *A;
	*A = *B;
	*B = Temp;
}

internal int
StringCompare(char *A, char *B)
{
	while((*A != 0) && (*A == *B))
	{
		++A, ++B;
	}

	return(*A - *B);
}

internal void
StringCopy(char *Destination, char *Source)
{
	while((*Destination = *Source) != 0)
	{
		++Destination, ++Source;
	}
}

internal void
ParseCommandLineOptions(int Argc, char **Argv)
{
	while(Argc-- > 1)
	{
		char *Command = *(++Argv);
		if(*Command == '-')
		{
			++Command; // NOTE(rick): Advance the pointer to the next character
			int Character = 0;
			while((Character = *Command++) != 0)
			{
				switch(Character)
				{
					case 'd':
					{
						char *Value = *(Argv + 1);
						if(Value != 0)
						{
							if(StringCompare(Value, "asc") == 0)
							{
								Config_Direction = SortDirection_Ascending;
							}
							else if(StringCompare(Value, "desc") == 0)
							{
								Config_Direction = SortDirection_Descending;
							}
						}
					} break;

					case 'p':
					{
						Config_ShowSettings = 1;
					}break;

					case 'h':
					{
						printf("Usage %s [options]\n", *Argv);
						printf("-d [asc/desc] - Sorting direction\n");
						printf("-h - Prints this help message\n");
						printf("-p - Prints configuration\n");
					}break;
					
					default:
					{
						fprintf(stderr, "Unknown switch %c\n", Character);
					} break;
				}
			}
		}
	}
}

static int
GetLine(char **Destination)
{
#define MAXLENGTH 1000
	int Length = 0;

	int Character = 0;
	char TempLine[MAXLENGTH];
	while(((TempLine[Length] = Character = getchar()) != EOF) &&
		  (Length++ < MAXLENGTH))
	{
		if(Character == '\n')
		{
			TempLine[Length - 1] = 0;
			break;
		}
	}

	if(Length > 0)
	{
		char *NewLine = (char *)malloc(sizeof(char) * Length);
		StringCopy(NewLine, TempLine);
		*Destination = NewLine;
	}

#undef MAXLENGTH
	return(Length);
}

internal void
QuickSort(char **Array, int Left, int Right)
{
	if(Left >= Right)
	{
		return;
	}

	Swap((void **)(Array + Left), (void **)(Array + ((Left + Right) / 2)));

	int Last = Left;
	for(int Index = Left + 1; Index <= Right; ++Index)
	{
		if(Config_Direction == SortDirection_Ascending)
		{
			if(StringCompare(*(Array + Index), *(Array + Left)) < 0)
			{
				Swap((void **)(Array + (++Last)), (void **)(Array + Index));
			}
		}
		else
		{
			if(StringCompare(*(Array + Index), *(Array + Left)) > 0)
			{
				Swap((void **)(Array + (++Last)), (void **)(Array + Index));
			}
		}
	}

	Swap((void **)(Array + Left), (void **)(Array + Last));
	QuickSort(Array, Left, Last - 1);
	QuickSort(Array, Left + 1, Right);
}

int
main(int Argc, char **Argv)
{
	ParseCommandLineOptions(Argc, Argv);
	if(Config_ShowSettings)
	{
		PrintConfig();
	}

	char **Lines = (char**)malloc(sizeof(char *) * Config_MaxLines);
	ZeroMemory((void *)Lines, sizeof(char *) * Config_MaxLines);
	int LinesIndex = 0;

	char *NewLine = 0;
	while((GetLine(&NewLine) > 0) &&
		  (LinesIndex < Config_MaxLines))
	{
		*(Lines + LinesIndex) = NewLine;
		++LinesIndex;
	}

	QuickSort(Lines, 0, LinesIndex - 1);
	for(int i = 0; i < LinesIndex; ++i)
	{
		printf("%s\n", *(Lines + i));
		free(*(Lines + i));
	}

	free(Lines);
	return 0;
}
