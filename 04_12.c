#include <stdio.h>

int
StringLength(char String[])
{
	int Length = 0;
	for(Length = 0; String[Length] != 0; ++Length);

	return(Length);
}

void
ZeroMemory(void *Mem, int Size)
{
	unsigned char *Ptr = Mem;
	while(Size--)
	{
		*Ptr++ = 0;
	}
}

void
IToA(int Value, char String[])
{
	if(Value == 0)
	{
		return;  // Unroll the stack once we've decimated the whole number
	}

	int IsNegative = (Value < 0);

	IToA(Value / 10, String);

	int Number = Value % 10;
	if(IsNegative)
	{
		Number = -Number;
	}
	int Character = Number + '0';

	int Length = StringLength(String);
	String[Length] = Character;
}

int main(void)
{
	char Output[32];
	ZeroMemory(Output, 32);
	IToA(-123456, Output);
	printf("%s\n", Output);

	ZeroMemory(Output, 32);
	IToA(1234567, Output);
	printf("%s\n", Output);
	return 0;
}
