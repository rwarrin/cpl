#include <stdio.h>
#include <stdlib.h>

#define MAXLINELENGTH 1000

static void
StringCopy(char *Target, char *Source)
{
	while((*Target++ = *Source++) != 0);
}

static int
GetLine(char **SaveTo)
{
	int Count = 0;
	int Character = 0;
	char Line[MAXLINELENGTH] = {0};
	while(((Line[Count] = Character = getchar()) != EOF) &&
		  (Count < MAXLINELENGTH))
	{
		++Count;
		if(Character == '\n')
		{
			Line[Count - 1] = 0;
			break;
		}
	}

	if(Count)
	{
		*SaveTo = (char *)malloc(sizeof(char) * Count);
		StringCopy(*SaveTo, Line);
	}
	else
	{
		*SaveTo = (char *)malloc(sizeof(char));
		*((*SaveTo) + 0) = 0;
	}

	return(Count);
}

static void
PrintWriteBackLines(char **Lines, int Count)
{
	for(int Index = Count - 1; Index >= 0; --Index)
	{
		printf("> %s\n", *(Lines + Index));
	}
}

int
main(int Argc, char **Argv)
{
	int WriteBackLines = 10;
	while(Argc-- > 0)
	{
		char *Argument = *Argv++;
		if(*Argument == '-')
		{
			++Argument;
			int Character = 0;
			while((Character = (*Argument++)) != 0)
			{
				switch(Character)
				{
					case 'n':
					{
						if(*Argv != 0)
						{
							WriteBackLines = atoi(*Argv);
						}
					} break;
					default:
					{
						fprintf(stderr, "Invalid switch %c\n", Character);
					} break;
				}
			}
		}
	}

	// NOTE(rick): This is our storage for lines read. Lines is a pointer to
	// pointer to char, meaning we'll treat this as a pointer to an array of
	// pointers to char.
	char **Lines = 0;
	Lines = (char **)malloc(sizeof(char *) * WriteBackLines);

	char *ReadLine = *Lines;
	int LinesRead = 0;
	while(GetLine(&ReadLine) > 0)
	{
		LinesRead = ((LinesRead + 1) % WriteBackLines);
		free(Lines[WriteBackLines - 1]);
		for(int LinesIndex = WriteBackLines - 1; LinesIndex >= 0; --LinesIndex)
		{
			*(Lines + LinesIndex) = *(Lines + LinesIndex - 1);
		}
		*Lines = ReadLine;
	}

	PrintWriteBackLines(Lines, WriteBackLines);
	for(int LinesIndex = 0; LinesIndex < WriteBackLines; ++LinesIndex)
	{
		free(*(Lines+LinesIndex));
	}
	free(*Lines);

	return 0;
}
