#include <stdio.h>

int main(void)
{
	int Character = 0;
	while((Character = getchar()) != EOF)
	{
		if((Character == '\n') ||
		   (Character == '\t') ||
		   (Character == ' '))
		{
			int NewCharacter = 0;
			while((NewCharacter = getchar()) == Character);
			putchar(Character);
			putchar(NewCharacter);
		}
		else
		{
			putchar(Character);
		}
	}

	return 0;
}
