#include <stdio.h>
#include <limits.h>

#define ArrayCount(Array) (sizeof(Array) / sizeof((Array)[0]))

// NOTE(rick): Returns the length including the NULL terminator
int
StringLength(char String[])
{
	int Length = 0;

	while(String[Length++] != 0);

	return(Length);
}

void
Reverse(char String[])
{
	int Length = StringLength(String);

	for(int Start = 0, End = Length - 2;
		Start < End;
		++Start, --End)
	{
		char Temp = String[Start];
		String[Start] = String[End];
		String[End] = Temp;
	}
}

void
IToA(int Number, char String[])
{
	int StringIndex = 0;

	int IsNegative = (Number < 0);

	while(Number != 0)
	{
		int Digit = (Number % 10);
		if(IsNegative)
		{
			Digit = -Digit;
		}

		String[StringIndex++] = Digit + '0';
		Number = Number / 10;
	}

	if(IsNegative)
	{
		String[StringIndex++] = '-';
	}

	String[StringIndex++] = 0;
	Reverse(String);
}

int main(void)
{
	char OutBuffer[32];
	int Number = INT_MIN;
	IToA(Number, OutBuffer);
	printf("%d\n%s\n", Number, OutBuffer);

	Number = INT_MAX;
	IToA(Number, OutBuffer);
	printf("%d\n%s\n", Number, OutBuffer);
	return 0;
}
