#include <stdio.h>

#define LENGTHLIMIT 80

int main(void)
{
	int Character = 0;
	int Length = 0;

	while((Character = getchar()) != EOF)
	{
		if(Character == '\n')
		{
			Length = 0;
		}

		if(Length == LENGTHLIMIT)
		{
			Length = 0;
			putchar('\n');
		}

		++Length;
		putchar(Character);
	}

	return 0;
}
