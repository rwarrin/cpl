#include <stdio.h>

#define CASE_MASK 0x20

#define IsAlpha(Character) ((Character >= 'a' && Character <= 'z') || \
							(Character >= 'A' && Character <= 'Z'))

static int
StringCompare(char *A, char *B)
{
	while((*A != 0) && (*A == *B))
	{
		++A, ++B;
	}

	return(*A - *B);
}

static void
ToLower()
{
	char Character = 0;
	while((Character = getchar()) != EOF)
	{
		if(IsAlpha(Character))
		{
			Character |= CASE_MASK;
		}

		putchar(Character);
	}
}

static void
ToUpper()
{
	char Character = 0;
	while((Character = getchar()) != EOF)
	{
		if(IsAlpha(Character))
		{
			Character &= ~CASE_MASK;
		}

		putchar(Character);
	}
}

int
main(int Argc, char **Argv)
{
	void (*Function)();
	if(StringCompare(Argv[0], "./lower") == 0)
	{
		Function = ToLower;
	}
	else if(StringCompare(Argv[0], "./upper") == 0)
	{
		Function = ToUpper;
	}
	else
	{
		fprintf(stderr, "Invalid operation\n");
		return 1;
	}

	Function();

	return 0;
}
