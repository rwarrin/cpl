#include <stdio.h>

int main(void)
{
	int lower = 0;
	int upper = 300;
	int step = 20;
	float fahr = 0.0f;

	printf("%3s %6s\n", "C", "F");
	float cel = lower;
	while(cel <= upper)
	{
		fahr = (9.0f/5.0f)*cel + 32.0f;
		printf("%3.0f %6.1f\n", cel, fahr);
		cel = cel + step;
	}


	return 0;
}
