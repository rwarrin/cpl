#include <stdio.h>

#define ArrayCount(Array) (sizeof(Array) / sizeof((Array)[0]))

int
BinarySearch(int Value, int Array[], int Min, int Max)
{
	if(Min > Max)
	{
		return -1;
	}

	int Mid = (Min + Max) / 2;
	if(Array[Mid] == Value)
	{
		return Mid;
	}

	if(Value < Array[Mid] )
	{
		return BinarySearch(Value, Array, Min, Mid - 1);
	}
	else if(Value > Array[Mid] )
	{
		return BinarySearch(Value, Array, Mid + 1, Max);
	}

	return -1;

}

int main(void)
{
	int Numbers[] = {1, 3, 4, 5, 8, 9};
	int Index = BinarySearch(1, Numbers, 0, ArrayCount(Numbers));
	printf("%d\n", Index);

	return 0;
}
