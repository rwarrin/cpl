#include <stdio.h>

#define LINELENGTH 1000

void
ZeroMemory(void *Memory, int Size)
{
	unsigned char *Ptr = (unsigned char *)Memory;
	while(--Size)
	{
		*Ptr++ = 0;
	}
}

int
GetLine(char Buffer[], int Limit)
{
	int Length = 0;
	int Character = 0;

	while((Character = getchar()) != EOF)
	{
		if(Character == '\n')
		{
			break;
		}
		Buffer[Length++] = Character;
	}

	return Length;
}

int
TrimLine(char Buffer[], int Length)
{
	for(Length = Length - 1;
		((Buffer[Length] == ' ') ||
		 (Buffer[Length] == '\t'));
		--Length)
	{
		Buffer[Length] = 0;
	}

	return Length;
}

int main(void)
{
	char LineBuffer[LINELENGTH];
	int Length = 0;

	ZeroMemory(LineBuffer, LINELENGTH);
	while((Length = GetLine(LineBuffer, LINELENGTH)) > 0)
	{
		Length = TrimLine(LineBuffer, Length);
		printf("%s\n", LineBuffer);
		ZeroMemory(LineBuffer, LINELENGTH);
	}

	return 0;
}
