#include <stdio.h>

int
IsDigit(char Character)
{
	int Result = ((Character >= '0') && (Character <= '9'));
	return(Result);
}

float AToF(char String[])
{
	int Value = 0;
	int IsNegative = 0;

	if(String[0] == '-')
	{
		IsNegative = 1;
	}

	int StringIndex = 0;
	if((String[0] == '-') || (String[0] == '+'))
	{
		++StringIndex;
	}

	int Left = 0;
	for(; IsDigit(String[StringIndex]); ++StringIndex)
	{
		Left = (Left * 10) + (String[StringIndex] - '0');
	}

	int Right = 0;
	int Power = 1;
	if(String[StringIndex] == '.')
	{
		++StringIndex;
		for(; IsDigit(String[StringIndex]); ++StringIndex)
		{
			Right = (Right * 10) + (String[StringIndex] - '0');
			Power *= 10;
		}
	}

	float Result = (float)Left + ((float)Right / (float)Power);
	if(IsNegative)
	{
		Result = -Result;
	}

	if((String[StringIndex] == 'e') || (String[StringIndex] == 'E'))
	{
		++StringIndex;

		int NegativeExponent = 0;
		if(String[StringIndex] == '-')
		{
			NegativeExponent = 1;
		}
		if((String[StringIndex] == '-') || (String[StringIndex] == '+'))
		{
			++StringIndex;
		}

		int Exponent = 0;
		for(; IsDigit(String[StringIndex]); ++StringIndex)
		{
			Exponent = (Exponent * 10) + (String[StringIndex] - '0');;
		}

		printf("%d\n", Exponent);

		if(NegativeExponent)
		{
			for(int i = 0; i < Exponent; ++i)
			{
				Result /= Result;
			}
		}
		else
		{
			for(int i = 0; i < Exponent; ++i)
			{
				Result *= Result;
			}
		}
	}

	return(Result);
}

int main(void)
{
	char Number[] = "123.4e2";
	float Value = AToF(Number);
	printf("%f\n", Value);

	return 0;
}
