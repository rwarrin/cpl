#include <stdio.h>

char
ToLower(char Letter)
{
	return ((Letter >= 'A' && Letter <= 'Z') ? (Letter - 'A') + 'a' : Letter);
}

int main(void)
{
	printf("%c\n", ToLower('C'));
	return 0;
}
