#include <stdio.h>
#include <stdlib.h>

#define STACKSIZE 100
double Stack[STACKSIZE] = {0};
int StackIndex = 0;

static int
StringCompare(char *A, char *B)
{
	while((*A != 0) && (*A == *B))
	{
		++A, ++B;
	}

	return *A - *B;
}

static void
Push(double Value)
{
	if(StackIndex >= STACKSIZE)
	{
		fprintf(stderr, "Stack is full, couldn't push %f\n", Value);
	}
	else
	{
		Stack[StackIndex++] = Value;
	}
}

static double
Pop()
{
	double Result = 0.0;

	if(StackIndex > 0)
	{
		Result = Stack[--StackIndex];
	}
	else
	{
		fprintf(stderr, "Stack is empty\n");
	}

	return(Result);
}

int main(int Argc, char *Argv[])
{

	int ArgIndex = 1;
	while(Argc-- > 1)
	{
		char *Operation = *(Argv + ArgIndex);
		if(StringCompare(Operation, "+") == 0)
		{
			double Result = Pop() + Pop();
			Push(Result);
		}
		else if(StringCompare(Operation, "-") == 0)
		{
			double Subtrahend = Pop();
			double Result = Pop() - Subtrahend;
			Push(Result);
		}
		else if(StringCompare(Operation, "*") == 0)
		{
			double Result = Pop() * Pop();
			Push(Result);
		}
		else if(StringCompare(Operation, "/") == 0)
		{
			double Divisor = Pop();
			if(Divisor != 0.0)
			{
				double Result = Pop() / Divisor;
				Push(Result);
			}
			else
			{
				fprintf(stderr, "Error: Divide by zero\n");
			}
		}
		else
		{
			double Value = atof(Operation);
			Push(Value);
		}
		ArgIndex++;
	}

	double Answer = Pop();
	printf("\t%.8g\n", Answer);

	return 0;
}
