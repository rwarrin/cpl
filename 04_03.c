// TODO(rick): We skipped practice items 4.8 - 4.10, we should come back to
// these at some point but they don't seem worthwhile at the moment.

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

enum OperationType
{
	OperationType_Invalid = -1,
	OperationType_Number,
	OperationType_String,
	OperationType_SetVariable,
	OperationType_GetVaraible,

	OperationType_Count,
};

int
IsDigit(char Value)
{
	int Result = 0;
	if((Value >= '0') && (Value <= '9'))
	{
		Result = 1;
	}

	return(Result);
}

int
IsAlpha(char Value)
{
	int Result = 0;
	if(((Value >= 'a') && (Value <= 'z')) ||
	   ((Value >= 'A') && (Value <= 'Z')))
	{
		Result = 1;
	}

	return(Result);
}

int
IsSpecial(char Value)
{
	int Result = 0;

	if(Value == '=')
	{
		Result = 1;
	}

	return(Result);
}

int
StringCompare(char A[], char B[])
{
	int AIndex = 0;
	int BIndex = 0;

	for(; A[AIndex] == B[BIndex]; ++AIndex, ++BIndex)
	{
		if(A[AIndex] == 0)
		{
			return 0;
		}
	}

	return(A[AIndex] - B[BIndex]);
}

int
StringLength(char String[])
{
	int Length = 0;
	for(; String[Length] != 0; ++Length);

	return(Length);
}

#define STACKSIZE 100

int StackIndex = 0;
double Stack[STACKSIZE];

void
Push(double Value)
{
	if(StackIndex < STACKSIZE)
	{
		Stack[StackIndex++] = Value;
	}
	else
	{
		fprintf(stderr, "Stack is full! Could not push %g\n", Value);
	}
}

double
Pop(void)
{
	double Result = 0.0;
	if(StackIndex > 0)
	{
		Result = Stack[--StackIndex];
	}

	return(Result);
}

void
Clear(void)
{
	while(StackIndex > 0)
	{
		Stack[StackIndex--] = 0;
	}
	printf("%d\n", StackIndex);
}

double
GetTopOfStack(void)
{
	double Result = 0.0;
	Result = Stack[StackIndex];

	return(Result);
}

#define BUFFERSIZE 100
char Buffer[BUFFERSIZE];
int BufferPosition = 0;

int
GetCh(void)
{
	int Result = 0;
	if(BufferPosition > 0)
	{
		Result = Buffer[--BufferPosition];
	}
	else
	{
		Result = getchar();
	}

	return(Result);
}

int
UngetCh(int Character)
{
	int Result = 0;
	if(BufferPosition >= BUFFERSIZE)
	{
		fprintf(stderr, "Buffer full, could not ungetch %c\n", Character);
	}
	else
	{
		Buffer[BufferPosition++] = Character;
		Result = 1;
	}

	return(Result);
}

int
Ungets(char String[])
{
	int Index = 0;
	for(Index = 0;
		String[Index] != 0;
		++Index)
	{
		if(UngetCh(String[Index]) == 0)
		{
			// NOTE(rick): Early out if we can't unget anymore
			break;
		}
	}

	return Index;
}

#define MAXVARIABLES 100
int VariableCount = 0;
double Variables[MAXVARIABLES];

int
HashVariableName(char Name[])
{
	int Hash = 0;
	for(int Index = 0, Length = StringLength(Name);
		Index < Length;
		++Index)
	{
		// TODO(rick): Better hash function!
		Hash = Hash*35 + Name[Index];
	}

	Hash = Hash % MAXVARIABLES;

	return(Hash);
}

void
SetVariable(char Name[], double Value)
{
	int NameHash = HashVariableName(Name);

	// NOTE(rick): This is a really terrible hash table, any collisions will
	// result in the previous value being overwritten. We really need a
	// structure to store the name and value inorder to perform some sort of
	// internal/external chaining to avoid collisions.
	Variables[NameHash] = Value;
}

double
GetVariable(char Name[])
{
	double Result = 0.0;
	int NameHash = HashVariableName(Name);
	Result = Variables[NameHash];

	return(Result);
}

int
GetOp(char StringOut[])
{
	// Eat whitespace
	int Character = 0;
	while(((StringOut[0] = Character = GetCh()) == ' ') ||
		  (Character == '\t'));
	StringOut[1] = 0;  // Add null terminator

	if((!IsDigit(Character)) &&
	   (!IsAlpha(Character)) &&
	   (!IsSpecial(Character)))
	{
		return Character;
	}

	int ReturnValue = OperationType_Invalid;
	int StringIndex = 0;
	if(IsSpecial(Character))
	{
		if(Character == '=')
		{
			StringOut[0] = GetCh();  // NOTE(rick): Store variable name after '='
			while((IsAlpha(StringOut[++StringIndex] = Character = GetCh())));
			ReturnValue = OperationType_SetVariable;
		}
	}
	else if(IsAlpha(Character))
	{
		while((IsAlpha(StringOut[++StringIndex] = Character = GetCh())));
		ReturnValue = OperationType_String;
	}
	else if(IsDigit(Character))
	{
		while((IsDigit(StringOut[++StringIndex] = Character = GetCh())));

		// handle floating point numbers
		if(Character == '.')
		{
			while((IsDigit(StringOut[++StringIndex] = Character = GetCh())));
		}

		ReturnValue = OperationType_Number;
	}

	StringOut[StringIndex] = 0;
	if(Character != EOF)
	{
		UngetCh(Character);
	}

#if 0
	printf("Read (%s)\n", StringOut);
#endif

	return ReturnValue;
}

int main(void)
{
	double LastResult = 0.0;

	int Type = 0;
	char Operation[BUFFERSIZE];
	while((Type = GetOp(Operation)) != EOF)
	{
		switch(Type)
		{
			case OperationType_Invalid:
			{
				fprintf(stderr, "Error: Invalid input %s\n", Operation);
			} break;

			case OperationType_Number:
			{
				double Value = atof(Operation);
				Push(Value);
			} break;

			case OperationType_String:
			{
				// Command: Clears the stack
				if(StringCompare(Operation, "c") == 0)
				{
					Clear();
				}

				// Command: Copy top of stack
				else if(StringCompare(Operation, "d") == 0)
				{
					double Result = GetTopOfStack();
					Push(Result);
				}

				// Command: Shows what is at the top of the stack
				else if(StringCompare(Operation, "p") == 0)
				{
					double Result = GetTopOfStack();
					printf("%.8g\n", Result);
				}
				
				// Command: Sin
				else if(StringCompare(Operation, "sin") == 0)
				{
					double Result = sin(Pop());
					Push(Result);
				}

				// Command: Cos
				else if(StringCompare(Operation, "cos") == 0)
				{
					double Result = cos(Pop());
					Push(Result);
				}

				// Command: Pow
				else if(StringCompare(Operation, "pow") == 0)
				{
					double Power = Pop();
					double Result = pow(Pop(), Power);
					Push(Result);
				}

				// Command: Exponent
				else if(StringCompare(Operation, "exp") == 0)
				{
					double Result = exp(Pop());
					Push(Result);
				}

				// Command: Ans recall last Result
				else if(StringCompare(Operation, "ans") == 0)
				{
					Push(LastResult);
				}

				// Note(rick): If we have a string and it doesn't match an
				// operation then assume it is a variable
				else
				{
					double Value = GetVariable(Operation);
					Push(Value);
				}

			} break;

			// Command: =<VariableName> Stores a value in a named variable
			case OperationType_SetVariable:
			{
				SetVariable(Operation, Pop());
			} break;

			// Command: Add
			case '+':
			{
				double Result = Pop() + Pop();
				Push(Result);
			} break;

			// Command: Subtract
			case '-':
			{
				double Subtrahend = Pop();
				double Result = Pop() - Subtrahend;
				Push(Result);
			} break;

			// Command: Multiply
			case '*':
			{
				double Result = Pop() * Pop();
				Push(Result);
			} break;

			// Command: Divide
			case '/':
			{
				double Divisor = Pop();
				if(Divisor == 0.0)
				{
					printf("Error: Divide by zero!\n");
					break;
				}
				else
				{
					double Result = Pop() / Divisor;
					Push(Result);
				}
			} break;

			// Command: Modulus
			case '%':
			{
				double Divisor = Pop();
				if(Divisor == 0.0)
				{
					printf("Error: Modulus by zero!\n");
					break;
				}
				else
				{
					double Result = (double)((int)Pop() % (int)Divisor);
					Push(Result);
				}
			} break;

			// Command: Evaluate and print result
			case '\n':
			{
				double Result = Pop();
				LastResult = Result;

				printf("\t%.8g\n", Result);
			} break;

			default:
			{
				printf("Error: Unknown operation '%s'\n", Operation);
			} break;
		}
	}

	return 0;
}
