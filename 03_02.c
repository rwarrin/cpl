#include <stdio.h>

#define ArrayCount(Array) (sizeof(Array) / sizeof((Array)[0]))
#define BUFFERLENGTH 1000

int
GetLine(char Buffer[], int Limit)
{
	int Length = 0;
	int Character = 0;

	while(((Character = getchar()) != EOF) &&
		  (Length < Limit - 1))
	{
		Buffer[Length++] = Character;
		if(Character == '\n')
		{
			break;
		}
	}

	if(Length)
	{
		Buffer[Length++] = 0;
	}

	return Length;
}

// NOTE(rick): Assumes Out has enough space to store the escaped string
int
Escape(char In[], char Out[])
{
	int InIndex = 0;
	int OutIndex = 0;
	
	while(In[InIndex] != 0)
	{
		char Character = In[InIndex];
		switch(Character)
		{
			case '\n':
			{
				Out[OutIndex++] = '\\';
				Out[OutIndex++] = 'n';
			} break;
			case '\t':
			{
				Out[OutIndex++] = '\\';
				Out[OutIndex++] = 't';
			} break;
			default:
			{
				Out[OutIndex++] = Character;
			} break;
		}
		++InIndex;
	}

	if(OutIndex)
	{
		Out[OutIndex++] = 0;
	}

	return OutIndex;
}

// NOTE(rick): Assumes Out has enough room for the unescaped string
int
Unescape(char In[], char Out[])
{
	int InIndex = 0;
	int OutIndex = 0;

	while(In[InIndex] != 0)
	{
		int Character = In[InIndex];
		if(Character == '\\')
		{
			Character = In[++InIndex];
			switch(Character)
			{
				case 'n':
				{
					Out[OutIndex++] = '\n';
				} break;
				case 't':
				{
					Out[OutIndex++] = '\t';
				} break;
				default:
				{
					Out[OutIndex++] = '\\';
					Out[OutIndex++] = Character;
				} break;
			}
		}
		else
		{
			Out[OutIndex++] = Character;
		}
		++InIndex;
	}

	if(OutIndex)
	{
		Out[OutIndex++] = 0;
	}

	return OutIndex;
}

int main(void)
{
	int InputLength = 0;
	char Input[BUFFERLENGTH];
	printf("String >");
	InputLength = GetLine(Input, ArrayCount(Input));

	int OutLength = 0;
	char Out[BUFFERLENGTH];
	OutLength = Escape(Input, Out);
	printf("%s\n", Out);

	InputLength = Unescape(Out, Input);
	printf("%s\n", Input);

	return 0;
}
