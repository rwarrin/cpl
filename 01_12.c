#include <stdio.h>

int main(void)
{
	int Character = 0;
	int Whitespace = 0;
	int Newline = 0;

	while((Character = getchar()) != EOF)
	{
		if((Character == '\t') ||
		   (Character == '\n') ||
		   (Character == ' '))
		{
			if((Character == ' ') ||
			   (Character == '\t'))
			{
				Newline = 1;
			}

			continue;
		}

		if(Newline)
		{
			putchar('\n');
			Newline = 0;
		}

		putchar(Character);
	}
	putchar('\n');

	return 0;
}
