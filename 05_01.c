#include <stdio.h>

#define PUSHBACKSIZE 100
int PushBackBuffer[PUSHBACKSIZE];
int PushBackIndex = 0;

int
GetCh(void)
{
	int Result = EOF;
	if(PushBackIndex > 0)
	{
		Result = PushBackBuffer[--PushBackIndex];
	}
	else
	{
		Result = getchar();
	}

	return(Result);
}

void
UngetCh(int Character)
{
	if(PushBackIndex < PUSHBACKSIZE)
	{
		PushBackBuffer[PushBackIndex++] = Character;
	}
	else
	{
		fprintf(stderr, "Could not unget %c\n", Character);
	}
}

int
IsSpace(char Character)
{
	int Result = 0;
	if((Character == ' ') ||
	   (Character == '\t'))
	{
		Result = 1;
	}

	return(Result);
}

int
IsDigit(char Character)
{
	int Result = 0;
	if((Result >= '0') && (Result <= '9'))
	{
		Result = 1;
	}

	return(Result);
}

int
GetInt(int *Number)
{
	int Character = 0;
	while(IsSpace(Character = GetCh()));

	if((!IsDigit(Character)) &&
	   (Character != EOF) &&
	   (Character != '+') &&
	   (Character != '-'))
	{
		UngetCh(Character);
		return 0;
	}

	int Sign = (Character == '-' ? -1 : 1);
	if((Character == '-') ||
	   (Character == '+'))
	{
		Character = GetCh();
	}

	for(*Number = 0; IsDigit(Character); Character = GetCh())
	{
		*Number = 10 * *Number + (Character - '0');
	}

	*Number *= Sign;
	if(Character != EOF)
	{
		UngetCh(Character);
	}

	return(Character);
}

#define MAXNUMBERS 100
int main(void)
{
	int Numbers[MAXNUMBERS];
	int NumbersIndex = 0;
	int Result = 0;
	while(((Result = GetInt(&Numbers[NumbersIndex])) != EOF) &&
		  (Result != 0))
	{
		++NumbersIndex;
	}
		

	for(int Index = 0;
		Index < NumbersIndex;
		++Index)
	{
		printf("%d\n", Numbers[Index]);
	}

	return 0;
}
