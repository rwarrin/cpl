#include <stdio.h>

#define ArrayCount(Array) (sizeof(Array) / sizeof((Array)[0]))
#define BUFFERLENGTH 1000

int
GetLine(char Buffer[], int Limit)
{
	int Length = 0;

	int Character = 0;
	while(((Character = getchar()) != EOF) && (Length < Limit - 1))
	{
		Buffer[Length++] = Character;
		if(Character == '\n')
		{
			break;
		}
	}

	if(Length > 0)
	{
		Buffer[Length++] = 0;
	}

	return(Length);
}

int
Squeeze(char String[], char Remove[])
{
	int StringCopyIndex = 0;

	for(int RemoveIndex = 0;
		Remove[RemoveIndex] != 0;
		++RemoveIndex)
	{
		StringCopyIndex = 0;

		for(int StringIndex = 0;
			String[StringIndex] != 0;
			++StringIndex)
		{
			char StringCharacter = String[StringIndex];
			char RemoveCharacter = Remove[RemoveIndex];

			if(StringCharacter != RemoveCharacter)
			{
				String[StringCopyIndex++] = StringCharacter;
			}
		}

		String[StringCopyIndex++] = 0;
	}

	return(StringCopyIndex);
}

int main(void)
{
	int StringLength = 0;
	int RemoveLength = 0;
	char String[BUFFERLENGTH];
	char Remove[BUFFERLENGTH];

	printf("String >");
	StringLength = GetLine(String, ArrayCount(String));

	printf("Characters to remove >");
	RemoveLength = GetLine(Remove, ArrayCount(Remove));
	// NOTE(rick): If there are characters to remove we need to remove the
	// newline character so it is not removed from the input string
	if(RemoveLength)
	{
		Remove[RemoveLength - 2] = 0;
	}

	StringLength = Squeeze(String, Remove);
	printf("%s", String);

	return 0;
}
