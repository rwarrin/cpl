#include <stdio.h>
#include <stdlib.h>

#define MAX_LINE 1000

static int
StringCompare(char *A, char *B)
{
	while((*A != 0) && (*A == *B))
	{
		++A, ++B;
	}

	return(*A - *B);
}

int
main(int Argc, char **Argv)
{
	if(Argc != 3)
	{
		printf("Usage: %s [first file] [second file]\n", Argv[0]);
		return 1;
	}

	FILE *First = fopen(Argv[1], "rb");
	if(First == NULL)
	{
		fprintf(stderr, "%s: Couldn't open '%s'\n", Argv[0], Argv[1]);
		return 2;
	}

	FILE *Second = fopen(Argv[2], "rb");
	if(Second == NULL)
	{
		fprintf(stderr, "%s: Couldn't open '%s'\n", Argv[0], Argv[2]);
		fclose(First);
		return 3;
	}

	char FirstLine[MAX_LINE];
	char SecondLine[MAX_LINE];
	int LineNumber = 0;

	int Running = 1;
	while(Running)
	{
		++LineNumber;
		char *FirstResult = fgets(FirstLine, MAX_LINE, First);
		char *SecondResult = fgets(SecondLine, MAX_LINE, Second);

		if(FirstResult && SecondResult)
		{
			if(StringCompare(FirstResult, SecondResult) != 0)
			{
				printf("Files differ on line %d\n", LineNumber);
				printf("%s(%d): %s", Argv[1], LineNumber, FirstResult);
				printf("%s(%d): %s", Argv[2], LineNumber, SecondResult);
				Running = 0;
			}
		}
		else
		{
			if(FirstResult)
			{
				printf("Reached end of '%s'\n", Argv[2]);
			}
			else if(SecondResult)
			{
				printf("Reached end of '%s'\n", Argv[1]);
			}
			else
			{
				printf("Files are identical\n");
			}

			Running = 0;
		}
	}

	fclose(First);
	fclose(Second);

	return 0;
}
