#include <stdio.h>
#include <stdlib.h>

#define ON 1
#define OFF 0

// Note(rick): We need to use this pragma to tell the compiler that we want our
// struct fields to be exactly the width we specify. What this means is this
// struct is 1 byte large where only the first 4 bits are used. Without this
// pragma this struct would consume 4 bytes!
#pragma pack(1)
struct some_flags
{
	// NOTE(rick): This is cool, using bitfields we are able to specify a nice
	// compact structure where a single bit is used per flag attribute.
	unsigned int SoundEnabled:1;
	unsigned int MusicEnabled:1;
	unsigned int ShowFPS:1;
	unsigned int InvertYAxis:1;
};
#pragma pack(0)

static void
PrintFlags(struct some_flags *Flags)
{
	printf("SoundEnabled = %s\n", (Flags->SoundEnabled ? "ON" : "OFF"));
	printf("MusicEnabled = %s\n", (Flags->MusicEnabled ? "ON" : "OFF"));
	printf("ShowFPS      = %s\n", (Flags->ShowFPS ? "ON" : "OFF"));
	printf("InvertYAxis  = %s\n", (Flags->InvertYAxis ? "ON" : "OFF"));
	printf("\n");
}

int
main(void)
{
	struct some_flags SomeFlags = {0};
	printf("%lu\n", sizeof(struct some_flags));
	PrintFlags(&SomeFlags);

	// NOTE(rick): We're able to set flags using standard mneomics like the
	// assignment operator.
	SomeFlags.SoundEnabled = ON;
	PrintFlags(&SomeFlags);

	SomeFlags.SoundEnabled = OFF;
	PrintFlags(&SomeFlags);

	SomeFlags.ShowFPS = SomeFlags.InvertYAxis = ON;
	PrintFlags(&SomeFlags);
	return 0;
}
