#include <stdio.h>

#define LINELENGTH 1000

int
GetLine(char Buffer[], int Limit)
{
	int Length = 0;
	int Character = 0;

	while((Character = getchar()) != EOF)
	{
		if(Character == '\n')
		{
			break;
		}

		Buffer[Length++] = Character;
	}

	if(Length > 0)
	{
		++Length;
		Buffer[Length] = 0;
	}

	return(Length);
}

void
Reverse(char String[], int Length)
{
	for(int Start = 0, End = Length - 2;
		Start < End;
		++Start, --End)
	{
		char Temp = String[Start];
		String[Start] = String[End];
		String[End] = Temp;
	}
}

void
ZeroMemory(void *Memory, int Size)
{
	unsigned char *Ptr = (unsigned char *)Memory;
	while(Size-- > 0)
	{
		*Ptr++ = 0;
	}
}

int main(void)
{
	char Buffer[LINELENGTH];
	int Length = 0;

	ZeroMemory(Buffer, LINELENGTH);
	while((Length = GetLine(Buffer, LINELENGTH)) > 0)
	{
		Reverse(Buffer, Length);
		printf("%s\n", Buffer);
		ZeroMemory(Buffer, LINELENGTH);
	}
	return 0;
}
