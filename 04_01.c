#include <stdio.h>

#define BUFFERSIZE 1000

int
GetLine(char Buffer[], int Limit)
{
	int Length = 0;

	int Character = 0;
	while((Limit-- > 0) && ((Character = getchar()) != EOF))
	{
		Buffer[Length++] = Character;
		if(Character == '\n')
		{
			break;
		}
	}

	if(Length)
	{
		Buffer[Length++] = 0;
	}

	return(Length);
}

int StrRIndex(char String[], char Find[])
{
	int Index = -1;

	for(int SIndex = 0; String[SIndex] != 0; ++SIndex)
	{
		int FIndex = 0;
		for(int FStart = SIndex;
			((Find[FIndex] != 0) && (String[FStart] == Find[FIndex]));
			++FStart, ++FIndex);

		if(Find[FIndex] == 0)
		{
			Index = SIndex;
		}
	}

	return(Index);
}

int main(void)
{
	char StrToFind[] = "ould";

	char Buffer[BUFFERSIZE];
	while((GetLine(Buffer, BUFFERSIZE)) > 2)
	{
		int Index = -1;
		Index = StrRIndex(Buffer, StrToFind);
		printf("%d\n", Index);
	}

	return 0;
}
