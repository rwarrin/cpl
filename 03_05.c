#include <stdio.h>

int
StringLength(char String[])
{
	int Length = 0;

	for(Length = 0; String[Length] != 0; ++Length);

	return(Length);
}

void
Reverse(char String[])
{
	int Length = StringLength(String);

	for(int Start = 0, End = Length - 1;
		Start < End;
		++Start, --End)
	{
		char Temp = String[Start];
		String[Start] = String[End];
		String[End] = Temp;
	}
}

char
GetBaseCharacter(int Value)
{
	char Result = -1;
	if(Value > 36)
	{
		return Result;
	}

	if((Value >= 0) && (Value <= 9))
	{
		Result = Value + '0';
	}
	if((Value >= 10) && (Value <= 36))
	{
		Result = Value + 'A';
	}

	return(Result);
}

void
IToB(unsigned int Number, char String[], int Base)
{
	int StringIndex = 0;

	while(Number != 0)
	{
		int Value = Number % Base;
		char Character = GetBaseCharacter(Value);
		if(Character < 0)
		{
			break;
		}

		String[StringIndex++] = Character;
		Number = (Number / Base);
	}

	String[StringIndex++] = 0;
	Reverse(String);
}

void
TestIToB(int Number, int Base)
{
	char OutBuffer[32];
	IToB(Number, OutBuffer, Base);
	printf("%d base %d = %s\n", Number, Base, OutBuffer);
}

int main(void)
{
	TestIToB(64, 16);
	TestIToB(20, 16);
	TestIToB(20, 2);
	TestIToB(50, 8);
	TestIToB(15, 3);
	TestIToB(14, 5);

	return 0;
}
