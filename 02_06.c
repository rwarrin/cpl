#include <stdio.h>

int
SetBits(int In, int Position, int Count, int From)
{
	int Result = 0;

	int MaskPosition = (Position - 1 - Count);
	if(MaskPosition < 0)
	{
		// NOTE(rick): Negative shifts are undefined behavior, so lets avoid
		// them
		MaskPosition = 0;
	}

	int Mask = (~(~0 << Count)) << MaskPosition;
	int CopiedBits = From & Mask;
	Result = In | CopiedBits;
	return(Result);
}

int main(void)
{
	int Result = SetBits(0x55, 4, 4, 0xFF);
	/*
	 * 0x55 = 0101 0101
	 * 0xFF = 1111 1111
	 * 4 bits from position 4 =
	 * 0x5F = 0101 1111
	 */
	printf("%d\n", Result);

	return 0;
}
